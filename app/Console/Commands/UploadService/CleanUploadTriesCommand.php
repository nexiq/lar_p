<?php

namespace App\Console\Commands\UploadService;

use App\Services\UploadService\UploadService;
use Illuminate\Console\Command;

class CleanUploadTriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uploadService:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удаляет мертвые файлы из сервиса загрузок';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        UploadService::cleanUploadTries(config('app.super_admin_email'));
    }
}
