<?php

namespace App\Console\Commands\Moderation;

use App\Services\Profile\ProfileModerationService;
use Illuminate\Console\Command;

class UpdateInactive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moderation:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновляет неактивные анкеты для модерации';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ProfileModerationService::updateInactiveProfiles();
    }
}
