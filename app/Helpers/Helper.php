<?php

namespace App\Helpers;

class Helper
{

    /**
     * Extracts bits from given integer value.
     *
     * @param     int $value
     * @return    array
     */
    public static function bitmask($value)
    {
        $bit = 1; // Equals to 0b00001
        $bits = [];
        // Check all bits less or equal to value
        while ($bit <= $value) {
            // This is bitwise AND, not boolean AND
            // 0b00001 & 0b11111 = 0b00001
            if (($bit & $value) != 0) {
                $bits[] = $bit;
            }
            // Shift bit to the left.
            // 0b00001 << 1 = 0b00010
            $bit = $bit << 1;
        }
        return $bits;
    }

    /**
     * Returns translated string(s) representing the value
     *
     * @param string $path
     * @param int $value
     * @param boolean $bitmask
     * @return string|string[]
     */
    public static function const($path, $value, $bitmask = false)
    {
        if (!$bitmask) {
            // Get string for given value
            $key = array_search($value, config("const.$path"));
            return trans("constants.$path.$key");
        } else {
            // Get string for each bit in mask
            $bits = self::bitmask($value);
            $strings = [];
            foreach ($bits as $bit) {
                $strings[] = self::const($path, $bit);
            }
            return $strings;
        }
    }
}
