<?php

namespace App\Providers;

use App\Services\Content\UploadService\ImageUploadService;
use App\Services\Content\UploadService\ImageUploadServiceInterface;
use App\Services\Content\UploadService\UploadService;
use App\Services\Content\UploadService\UploadServiceInterface;
use Illuminate\Support\ServiceProvider;

class UploadServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(UploadServiceInterface::class, function () {
            return (new UploadService())->setExceptionDescriptions(__('exceptions.uploadService'));
        });
        $this->app->singleton(ImageUploadServiceInterface::class, function () {
            return (new ImageUploadService())->setExceptionDescriptions(__('exceptions.uploadService'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
