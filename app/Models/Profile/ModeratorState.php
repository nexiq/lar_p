<?php

namespace App\Models\Profile;

use App\Models\Core\User;
use Illuminate\Database\Eloquent\Model;

class ModeratorState extends Model
{
    protected $fillable = ['user_id', 'start_at'];
    public function moderator()
    {
        return $this->belongsTo(User::class);
    }
}
