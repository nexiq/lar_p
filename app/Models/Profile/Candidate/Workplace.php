<?php

namespace App\Models\Profile\Candidate;

use App\Models\Profile\CandidateProfile;
use App\Models\Work\Profarea;
use App\Models\Work\Position;
use App\Models\Work\Skill;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Workplace extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'profarea_id',
        'position_id',
        'duration',
        'has_education',
        'has_recommendation',
        'recommender_phone',
        'recommender_phone_ext',
    ];

    /**
     * Профиль кандидата
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(CandidateProfile::class);
    }

    /**
     * Профобласть
     * @return BelongsTo
     */
    public function profarea()
    {
        return $this->belongsTo(Profarea::class);
    }

    /**
     * Должность
     * @return BelongsTo
     */
    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    /**
     * Отработанные навыки
     * @return HasMany
     */
    public function skills()
    {
        return $this->hasMany(
            Skill::class,
            'workplase_skills',
            'id',
            'skill_id'
        );
    }
}
