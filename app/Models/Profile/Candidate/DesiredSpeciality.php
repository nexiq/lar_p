<?php

namespace App\Models\Profile\Candidate;

use App\Models\Profile\CandidateProfile;
use App\Models\Work\Speciality;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DesiredSpeciality extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
        'experience_type',
        'education_type',
        'speciality_id',
    ];

    /**
     * Профиль кандидата
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(CandidateProfile::class);
    }

    /**
     * Профобласть
     * @return BelongsTo
     */
    public function speciality()
    {
        return $this->belongsTo(Speciality::class);
    }
}
