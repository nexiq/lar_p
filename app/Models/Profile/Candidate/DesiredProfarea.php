<?php

namespace App\Models\Profile\Candidate;

use App\Models\Profile\CandidateProfile;
use App\Models\Work\Profarea;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DesiredProfarea extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
        'experience_type',
        'education_type',
        'profarea_id',
    ];

    /**
     * Профиль кандидата
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(CandidateProfile::class);
    }

    /**
     * Профобласть
     * @return BelongsTo
     */
    public function profarea()
    {
        return $this->belongsTo(Profarea::class);
    }
}
