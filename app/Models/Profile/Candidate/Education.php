<?php

namespace App\Models\Profile\Candidate;

use App\Models\Profile\CandidateProfile;
use App\Models\Work\Speciality;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Education extends Model
{
    public $timestamps = false;

    protected $table = 'educations';

    protected $fillable = [
        'type',
        'form',
        'place_name',
        'speciality_id',
        'graduate_year',
        'comment',
    ];

    /**
     * Профиль кандидата
     * @return BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo(CandidateProfile::class);
    }

    /**
     * Специальность
     * @return BelongsTo
     */
    public function speciality()
    {
        return $this->belongsTo(Speciality::class);
    }
}
