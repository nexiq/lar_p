<?php

namespace App\Models\Profile;

use App\Models\Content\File;
use App\Models\Core\HasPersonalData;
use App\Models\Core\HasPersonalDataInterface;
use App\Models\Core\User;
use App\Models\Profile\Candidate\DesiredProfarea;
use App\Models\Profile\Candidate\DesiredSpeciality;
use App\Models\Profile\Candidate\Workplace;
use App\Models\Profile\Candidate\Education;
use App\Models\Work\City;
use App\Models\Work\Quality;
use App\Models\Work\Skill;
use App\Models\Work\Language;
use App\Services\Profile\Constants;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

use Carbon\Carbon;

class CandidateProfile extends Model implements HasPersonalDataInterface
{
    use HasPersonalData;

    /**
     * Перечень полей которые относятся к персональным данным
     */
    protected $hiddenPersonalDataAttributes = [
        'first_name',
        'last_name',
        'middle_name',
        'resumeFile',
        'avatarFile',
    ];
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'middle_name',
        'dob',
        'sex',
        'is_smoking',
        'has_medical_book',
        'can_work_in_russia',

        'height',
        'weight',
        'clothing_size',
        'shoe_size',
        'body_type',

        'city_id',

        'salary_monthly',
        'salary_daily',
        'salary_hourly',
        'work_type',
        'work_schedule',
        'work_team',

        'work_relocation_ready',
        'relocation_city_id',

        'trip_ready',
        'trip_city_id',
        'drivers_license',
    ];

    public function setDobAttribute($value)
    {
        $this->attributes['dob'] = Carbon::createFromFormat('d.m.Y', $value);
    }

    public function getDobAttribute($value)
    {
        $date = ($value instanceof Carbon) ? $value : Carbon::create($value);
        return $date->format('d.m.Y');
    }

    public function getAgeAttribute()
    {
        return (Carbon::now())->diffInYears($this->dob);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function relocationCity()
    {
        return $this->belongsTo(City::class);
    }

    public function tripCity()
    {
        return $this->belongsTo(City::class);
    }

    /** Связь к файлу резюме
     * @return BelongsTo
     */
    public function resumeFile()
    {
        return $this->belongsTo(File::class, 'resume_file_id');
    }

    public function avatarFile()
    {
        return $this->belongsTo(File::class, 'avatar_file_id');
    }

    /**
     * Список всех навыков
     * @return BelongsToMany
     */
    public function skills()
    {
        return $this->belongsToMany(
            Skill::class,
            'candidate_profile_skills',
            'candidate_profile_id',
            'skill_id'
        );
    }

    /**
     * Список всех лич. качеств
     * @return BelongsToMany
     */
    public function qualities()
    {
        return $this->belongsToMany(
            Quality::class,
            'candidate_profile_qualities',
            'candidate_profile_id',
            'quality_id'
        );
    }

    /**
     * Желаемые профобласти
     * @return HasMany
     */
    public function desiredProfareas()
    {
        return $this->hasMany(DesiredProfarea::class);
    }

    /**
     * Желаемые специальности
     * @return HasMany
     */
    public function desiredSpecialities()
    {
        return $this->hasMany(DesiredSpeciality::class);
    }

    /**
     * Места работы
     * @return HasMany
     */
    public function workplaces()
    {
        return $this->hasMany(Workplace::class);
    }

    /**
     * Места обучения
     * @return HasMany
     */
    public function educations()
    {
        return $this->hasMany(Education::class);
    }

    /**
     * Владение языками
     * @return BelongsToMany
     */
    public function languages()
    {
        return $this->belongsToMany(
            Language::class,
            'candidate_profile_languages',
            'candidate_profile_id',
            'language_id'
        );
    }

    public function moderation()
    {
        return $this->hasOne(ProfileModeration::class, 'candidate_profile_id', 'id');
    }
}
