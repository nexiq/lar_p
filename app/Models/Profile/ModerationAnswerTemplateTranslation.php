<?php

namespace App\Models\Profile;

use Illuminate\Database\Eloquent\Model;

class ModerationAnswerTemplateTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'moderation_answer_template_id', 'locale'];
}
