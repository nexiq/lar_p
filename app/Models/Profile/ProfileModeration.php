<?php

namespace App\Models\Profile;

use App\Models\Core\User;
use Illuminate\Database\Eloquent\Model;

class ProfileModeration extends Model
{
    protected $fillable = ['candidate_profile_id', 'user_id', 'deadline_at'];
    public function moderator()
    {
        return $this->belongsTo(User::class);
    }

    public function profile()
    {
        return $this->belongsTo(CandidateProfile::class, 'candidate_profile_id', 'id');
    }
}
