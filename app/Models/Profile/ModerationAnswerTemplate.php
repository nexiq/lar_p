<?php

namespace App\Models\Profile;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ModerationAnswerTemplate extends Model
{
    use Translatable;

    public $translatedAttributes = ['name'];
}
