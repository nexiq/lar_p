<?php

namespace App\Models\Profile\Policies;

use App\Models\Core\User;
use App\Models\Profile\CandidateProfile;
use Illuminate\Auth\Access\HandlesAuthorization;

class CandidateProfilePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\Profile\CandidateProfile $profile
     * @return mixed
     */
    public function viewAny(CandidateProfile $profile)
    {
        return \Entrust::can('profile.candidates.read');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Profile\CandidateProfile $model
     * @return mixed
     */
    public function view(User $user, CandidateProfile $model)
    {
        return $user->id == $model->user_id || \Entrust::can('profile.candidates.read');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\Core\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('profile.candidates.create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Profile\CandidateProfile $model
     * @return mixed
     */
    public function update(User $user, CandidateProfile $model)
    {
        return $user->id == $model->user_id || \Entrust::can('profile.candidates.update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Profile\CandidateProfile $model
     * @return mixed
     */
    public function delete(User $user, CandidateProfile $model)
    {
        return \Entrust::can('profile.candidates.delete');
    }

    /**
     * Определяет политику просмотра ПД
     * @param User $user
     * @param CandidateProfile $model
     * @return bool
     */
    public function viewPersonalData(User $user, CandidateProfile $model)
    {
        return $user->id == $model->user_id || \Entrust::can('core.personal.read');
    }
}
