<?php

namespace App\Models\Core;

interface HasPersonalDataInterface
{
    /**
     * Скрывает некоторые аттрибуты определенные в модели как $hiddenPersonalDataAttributes = [];
     * @return mixed
     */
    public function hidePersonalDataAttributes();
}
