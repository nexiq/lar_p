<?php

namespace App\Models\Core;

use App\Models\Content\File;
use App\Models\Profile\CandidateProfile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable implements HasPersonalDataInterface
{
    use Notifiable;
    use EntrustUserTrait;

    use HasPersonalData;
    protected $hiddenPersonalDataAttributes = [
        'name',
        'email',
        'phone',
        'phone_ext',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'api_token',
        'phone',
        'locale',
        'phone_ext',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Возвращает все файлы которые пользователь загружал
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = preg_replace('/[^0-9]/', '', $value);
    }

    /**
     * Профиль кандидата
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function candidateProfile()
    {
        return $this->hasOne(CandidateProfile::class);
    }
}
