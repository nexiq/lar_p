<?php

namespace App\Models\Core\Policies;

use App\Models\Core\User;
use App\Models\Core\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any permissions.
     *
     * @param \App\Models\Core\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('core.permissions.read');
    }

    /**
     * Determine whether the user can view the permission.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\Permission $permission
     * @return mixed
     */
    public function view(User $user, Permission $permission)
    {
        return \Entrust::can('core.permissions.read');
    }

    /**
     * Determine whether the user can create permissions.
     *
     * @param \App\Models\Core\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('core.permissions.create');
    }

    /**
     * Determine whether the user can update the permission.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\Permission $permission
     * @return mixed
     */
    public function update(User $user, Permission $permission)
    {
        return \Entrust::can('core.permissions.update');
    }

    /**
     * Determine whether the user can delete the permission.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\Permission $permission
     * @return mixed
     */
    public function delete(User $user, Permission $permission)
    {
        return \Entrust::can('core.permissions.delete');
    }

    /**
     * Determine whether the user can restore the permission.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\Permission $permission
     * @return mixed
     */
    public function restore(User $user, Permission $permission)
    {
        return \Entrust::can('core.permissions.delete');
    }

    /**
     * Determine whether the user can permanently delete the permission.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\Permission $permission
     * @return mixed
     */
    public function forceDelete(User $user, Permission $permission)
    {
        return \Entrust::can('core.permissions.delete');
    }
}
