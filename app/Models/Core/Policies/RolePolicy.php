<?php

namespace App\Models\Core\Policies;

use App\Models\Core\User;
use App\Models\Core\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any roles.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('core.roles.read');
    }

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Core\Role  $role
     * @return mixed
     */
    public function view(User $user, Role $role)
    {
        return \Entrust::can('core.roles.read');
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('core.roles.create');
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Core\Role  $role
     * @return mixed
     */
    public function update(User $user, Role $role)
    {
        return \Entrust::can('core.roles.update');
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Core\Role  $role
     * @return mixed
     */
    public function delete(User $user, Role $role)
    {
        return \Entrust::can('core.roles.delete');
    }

    /**
     * Determine whether the user can restore the role.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Core\Role  $role
     * @return mixed
     */
    public function restore(User $user, Role $role)
    {
        return \Entrust::can('core.roles.delete');
    }

    /**
     * Determine whether the user can permanently delete the role.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Core\Role  $role
     * @return mixed
     */
    public function forceDelete(User $user, Role $role)
    {
        return \Entrust::can('core.roles.delete');
    }
}
