<?php

namespace App\Models\Core\Policies;

use App\Models\Core\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param \App\Models\Core\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('core.users.read');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\User $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        return $user->id == $model->id || \Entrust::can('core.users.read');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param \App\Models\Core\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('core.users.create');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\User $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        return $user->id == $model->id || \Entrust::can('core.users.update');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\User $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        return \Entrust::can('core.users.delete');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\User $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        return \Entrust::can('core.users.delete');
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Core\User $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        return \Entrust::can('core.users.delete');
    }

    /**
     * Определяет политику просмотра ПД
     *
     * @param User $user
     * @param User $target
     * @return bool
     */
    public function viewPersonalData(User $user, User $target)
    {
        return $user->id == $target->id || \Entrust::can('core.personal.read');
    }
}
