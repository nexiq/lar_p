<?php

namespace App\Models\Core;

trait HasPersonalData
{

    /**
     * Функция скрывает аттрибуты модели
     * @return $this
     */
    public function hidePersonalDataAttributes()
    {
        if (!property_exists($this, 'hiddenPersonalDataAttributes') || empty($this->hiddenPersonalDataAttributes)) {
            return $this;
        }
        foreach ($this->hiddenPersonalDataAttributes as $attribute) {
            $this->$attribute = null;
        }
        return $this;
    }
}
