<?php

namespace App\Models\Content;

use App\Models\Core\User;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $hidden = [
        'local_path', // локальный, технический путь до файла, публике знать его не надо
    ];

    /**
     * Создатель файла
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
