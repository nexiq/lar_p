<?php

namespace App\Models\Content\Policies;

use App\Models\Core\User;
use App\Models\Content\Page;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any pages.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('content.pages.read');
    }

    /**
     * Determine whether the user can view the page.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Content\Page  $page
     * @return mixed
     */
    public function view(User $user, Page $page)
    {
        return \Entrust::can('content.pages.read');
    }

    /**
     * Determine whether the user can create pages.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('content.pages.create');
    }

    /**
     * Determine whether the user can update the page.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Content\Page  $page
     * @return mixed
     */
    public function update(User $user, Page $page)
    {
        return \Entrust::can('content.pages.update');
    }

    /**
     * Determine whether the user can delete the page.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Content\Page  $page
     * @return mixed
     */
    public function delete(User $user, Page $page)
    {
        return \Entrust::can('content.pages.delete');
    }

    /**
     * Determine whether the user can restore the page.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Content\Page  $page
     * @return mixed
     */
    public function restore(User $user, Page $page)
    {
        return \Entrust::can('content.pages.delete');
    }

    /**
     * Determine whether the user can permanently delete the page.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Content\Page  $page
     * @return mixed
     */
    public function forceDelete(User $user, Page $page)
    {
        return \Entrust::can('content.pages.delete');
    }
}
