<?php

namespace App\Models\Content\Policies;

use App\Models\Content\Translation;
use App\Models\Core\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TranslationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any translations.
     *
     * @param \App\Models\Core\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('content.translations.read');
    }

    /**
     * Determine whether the user can view the translation.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Content\Translation $translation
     * @return mixed
     */
    public function view(User $user)
    {
        return \Entrust::can('content.translations.read');
    }


    /**
     * Determine whether the user can update the translation.
     *
     * @param \App\Models\Core\User $user
     * @param string $translation 2х буквенное обозначение локали
     * @return mixed
     */
    public function update(User $user)
    {
        return \Entrust::can('content.translations.update');
    }
}
