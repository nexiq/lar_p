<?php

namespace App\Models\Content\Policies;

use App\Models\Core\User;
use App\Models\Content\File;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any files.
     *
     * @param \App\Models\Core\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('content.files.read');
    }

    /**
     * Determine whether the user can view the file.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Content\File $file
     * @return mixed
     */
    public function view(User $user, File $file)
    {
        return \Entrust::can('content.files.read');
    }

    /**
     * Determine whether the user can create files.
     *
     * @param \App\Models\Core\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('content.files.create');
    }

    /**
     * Determine whether the user can update the file.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Content\File $file
     * @return mixed
     */
    public function update(User $user, File $file)
    {
        return $user->id == $file->user_id || \Entrust::can('content.files.update');
    }

    /**
     * Determine whether the user can delete the file.
     *
     * @param \App\Models\Core\User $user
     * @param \App\Models\Content\File $file
     * @return mixed
     */
    public function delete(User $user, File $file)
    {
        return $user->id == $file->user_id || \Entrust::can('content.files.delete');
    }
}
