<?php

namespace App\Models\Content;

use App\Services\Content\UploadService\HasContentFile;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UploadTry extends Model
{
    //
    use HasContentFile;

    protected $fillable = [
        'file_id',
        'model',
        'field',
    ];

    public function scopeRecent($q)
    {
        if (config('app.env') == 'testing') {
            return $q;
        }
        return $this->where('created_at', '<', Carbon::now()->subDay());
    }
}
