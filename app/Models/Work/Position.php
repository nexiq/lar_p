<?php

namespace App\Models\Work;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Position extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['title'];
}
