<?php

namespace App\Models\Work;

use Illuminate\Database\Eloquent\Model;

class LanguageTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'language_id', 'locale'];
}
