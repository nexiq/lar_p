<?php

namespace App\Models\Work;

use Illuminate\Database\Eloquent\Model;

class PositionTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'position_id', 'locale'];
}
