<?php

namespace App\Models\Work;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class City extends Model implements TranslatableContract
{
    use Translatable;

    public $translatedAttributes = ['name'];

    protected $fillable = ['region_id'];

    public function getFullNameAttribute()
    {
        $region = $this->region ? ", " . $this->region->name : '';
        $country = ($this->region && $this->region->country) ? ", " . $this->region->country->name : '';
        return $this->name . $region . $country;
    }

    /**
     * Регион
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
