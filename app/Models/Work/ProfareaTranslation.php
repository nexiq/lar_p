<?php

namespace App\Models\Work;

use Illuminate\Database\Eloquent\Model;

class ProfareaTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'profarea_id', 'locale'];
}
