<?php

namespace App\Models\Work\Policies;

use App\Models\Core\User;
use App\Models\Work\Speciality;
use Illuminate\Auth\Access\HandlesAuthorization;

class SpecialityPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any specialities.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('work.specialities.read');
    }

    /**
     * Determine whether the user can view the speciality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Speciality  $speciality
     * @return mixed
     */
    public function view(User $user, Speciality $speciality)
    {
        return \Entrust::can('work.specialities.read');
    }

    /**
     * Determine whether the user can create specialities.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('work.specialities.create');
    }

    /**
     * Determine whether the user can update the speciality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Speciality  $speciality
     * @return mixed
     */
    public function update(User $user, Speciality $speciality)
    {
        return \Entrust::can('work.specialities.update');
    }

    /**
     * Determine whether the user can delete the speciality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Speciality  $speciality
     * @return mixed
     */
    public function delete(User $user, Speciality $speciality)
    {
        return \Entrust::can('work.specialities.delete');
    }

    /**
     * Determine whether the user can restore the speciality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Speciality  $speciality
     * @return mixed
     */
    public function restore(User $user, Speciality $speciality)
    {
        return \Entrust::can('work.specialities.delete');
    }

    /**
     * Determine whether the user can permanently delete the speciality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Speciality  $speciality
     * @return mixed
     */
    public function forceDelete(User $user, Speciality $speciality)
    {
        return \Entrust::can('work.specialities.delete');
    }
}
