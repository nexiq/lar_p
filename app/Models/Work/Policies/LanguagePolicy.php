<?php

namespace App\Models\Work\Policies;

use App\Models\Core\User;
use App\Models\Work\Language;
use Illuminate\Auth\Access\HandlesAuthorization;

class LanguagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any languages.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('work.languages.read');
    }

    /**
     * Determine whether the user can view the language.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Language  $language
     * @return mixed
     */
    public function view(User $user, Language $language)
    {
        return \Entrust::can('work.languages.read');
    }

    /**
     * Determine whether the user can create languages.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('work.languages.create');
    }

    /**
     * Determine whether the user can update the language.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Language  $language
     * @return mixed
     */
    public function update(User $user, Language $language)
    {
        return \Entrust::can('work.languages.update');
    }

    /**
     * Determine whether the user can delete the language.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Language  $language
     * @return mixed
     */
    public function delete(User $user, Language $language)
    {
        return \Entrust::can('work.languages.delete');
    }

    /**
     * Determine whether the user can restore the language.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Language  $language
     * @return mixed
     */
    public function restore(User $user, Language $language)
    {
        return \Entrust::can('work.languages.delete');
    }

    /**
     * Determine whether the user can permanently delete the language.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Language  $language
     * @return mixed
     */
    public function forceDelete(User $user, Language $language)
    {
        return \Entrust::can('work.languages.delete');
    }
}
