<?php

namespace App\Models\Work\Policies;

use App\Models\Core\User;
use App\Models\Work\Country;
use Illuminate\Auth\Access\HandlesAuthorization;

class CountryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any countries.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('work.countries.read');
    }

    /**
     * Determine whether the user can view the country.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Country  $country
     * @return mixed
     */
    public function view(User $user, Country $country)
    {
        return \Entrust::can('work.countries.read');
    }

    /**
     * Determine whether the user can create countries.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('work.countries.create');
    }

    /**
     * Determine whether the user can update the country.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Country  $country
     * @return mixed
     */
    public function update(User $user, Country $country)
    {
        return \Entrust::can('work.countries.update');
    }

    /**
     * Determine whether the user can delete the country.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Country  $country
     * @return mixed
     */
    public function delete(User $user, Country $country)
    {
        return \Entrust::can('work.countries.delete');
    }

    /**
     * Determine whether the user can restore the country.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Country  $country
     * @return mixed
     */
    public function restore(User $user, Country $country)
    {
        return \Entrust::can('work.countries.delete');
    }

    /**
     * Determine whether the user can permanently delete the country.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Country  $country
     * @return mixed
     */
    public function forceDelete(User $user, Country $country)
    {
        return \Entrust::can('work.countries.delete');
    }
}
