<?php

namespace App\Models\Work\Policies;

use App\Models\Core\User;
use App\Models\Work\Position;
use Illuminate\Auth\Access\HandlesAuthorization;

class PositionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any positions.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('work.positions.read');
    }

    /**
     * Determine whether the user can view the position.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Position  $position
     * @return mixed
     */
    public function view(User $user, Position $position)
    {
        return \Entrust::can('work.positions.read');
    }

    /**
     * Determine whether the user can create positions.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('work.positions.create');
    }

    /**
     * Determine whether the user can update the position.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Position  $position
     * @return mixed
     */
    public function update(User $user, Position $position)
    {
        return \Entrust::can('work.positions.update');
    }

    /**
     * Determine whether the user can delete the position.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Position  $position
     * @return mixed
     */
    public function delete(User $user, Position $position)
    {
        return \Entrust::can('work.positions.delete');
    }

    /**
     * Determine whether the user can restore the position.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Position  $position
     * @return mixed
     */
    public function restore(User $user, Position $position)
    {
        return \Entrust::can('work.positions.delete');
    }

    /**
     * Determine whether the user can permanently delete the position.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Position  $position
     * @return mixed
     */
    public function forceDelete(User $user, Position $position)
    {
        return \Entrust::can('work.positions.delete');
    }
}
