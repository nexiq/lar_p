<?php

namespace App\Models\Work\Policies;

use App\Models\Core\User;
use App\Models\Work\Skill;
use Illuminate\Auth\Access\HandlesAuthorization;

class SkillPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any skills.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('work.skills.read');
    }

    /**
     * Determine whether the user can view the skill.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Skill  $skill
     * @return mixed
     */
    public function view(User $user, Skill $skill)
    {
        return \Entrust::can('work.skills.read');
    }

    /**
     * Determine whether the user can create skills.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('work.skills.create');
    }

    /**
     * Determine whether the user can update the skill.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Skill  $skill
     * @return mixed
     */
    public function update(User $user, Skill $skill)
    {
        return \Entrust::can('work.skills.update');
    }

    /**
     * Determine whether the user can delete the skill.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Skill  $skill
     * @return mixed
     */
    public function delete(User $user, Skill $skill)
    {
        return \Entrust::can('work.skills.delete');
    }

    /**
     * Determine whether the user can restore the skill.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Skill  $skill
     * @return mixed
     */
    public function restore(User $user, Skill $skill)
    {
        return \Entrust::can('work.skills.delete');
    }

    /**
     * Determine whether the user can permanently delete the skill.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Skill  $skill
     * @return mixed
     */
    public function forceDelete(User $user, Skill $skill)
    {
        return \Entrust::can('work.skills.delete');
    }
}
