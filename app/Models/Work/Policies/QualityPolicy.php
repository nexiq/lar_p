<?php

namespace App\Models\Work\Policies;

use App\Models\Core\User;
use App\Models\Work\Quality;
use Illuminate\Auth\Access\HandlesAuthorization;

class QualityPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any qualities.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('work.qualities.read');
    }

    /**
     * Determine whether the user can view the quality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Quality  $quality
     * @return mixed
     */
    public function view(User $user, Quality $quality)
    {
        return \Entrust::can('work.qualities.read');
    }

    /**
     * Determine whether the user can create qualities.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('work.qualities.create');
    }

    /**
     * Determine whether the user can update the quality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Quality  $quality
     * @return mixed
     */
    public function update(User $user, Quality $quality)
    {
        return \Entrust::can('work.qualities.update');
    }

    /**
     * Determine whether the user can delete the quality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Quality  $quality
     * @return mixed
     */
    public function delete(User $user, Quality $quality)
    {
        return \Entrust::can('work.qualities.delete');
    }

    /**
     * Determine whether the user can restore the quality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Quality  $quality
     * @return mixed
     */
    public function restore(User $user, Quality $quality)
    {
        return \Entrust::can('work.qualities.delete');
    }

    /**
     * Determine whether the user can permanently delete the quality.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Quality  $quality
     * @return mixed
     */
    public function forceDelete(User $user, Quality $quality)
    {
        return \Entrust::can('work.qualities.delete');
    }
}
