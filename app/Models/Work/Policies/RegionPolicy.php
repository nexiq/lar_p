<?php

namespace App\Models\Work\Policies;

use App\Models\Core\User;
use App\Models\Work\Region;
use Illuminate\Auth\Access\HandlesAuthorization;

class RegionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any regions.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('work.regions.read');
    }

    /**
     * Determine whether the user can view the region.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Region  $region
     * @return mixed
     */
    public function view(User $user, Region $region)
    {
        return \Entrust::can('work.regions.read');
    }

    /**
     * Determine whether the user can create regions.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('work.regions.create');
    }

    /**
     * Determine whether the user can update the region.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Region  $region
     * @return mixed
     */
    public function update(User $user, Region $region)
    {
        return \Entrust::can('work.regions.update');
    }

    /**
     * Determine whether the user can delete the region.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Region  $region
     * @return mixed
     */
    public function delete(User $user, Region $region)
    {
        return \Entrust::can('work.regions.delete');
    }

    /**
     * Determine whether the user can restore the region.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Region  $region
     * @return mixed
     */
    public function restore(User $user, Region $region)
    {
        return \Entrust::can('work.regions.delete');
    }

    /**
     * Determine whether the user can permanently delete the region.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Region  $region
     * @return mixed
     */
    public function forceDelete(User $user, Region $region)
    {
        return \Entrust::can('work.regions.delete');
    }
}
