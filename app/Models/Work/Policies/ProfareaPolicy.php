<?php

namespace App\Models\Work\Policies;

use App\Models\Core\User;
use App\Models\Work\Profarea;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProfareaPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any profareas.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return \Entrust::can('work.profareas.read');
    }

    /**
     * Determine whether the user can view the profarea.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Profarea  $profarea
     * @return mixed
     */
    public function view(User $user, Profarea $profarea)
    {
        return \Entrust::can('work.profareas.read');
    }

    /**
     * Determine whether the user can create profareas.
     *
     * @param  \App\Models\Core\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return \Entrust::can('work.profareas.create');
    }

    /**
     * Determine whether the user can update the profarea.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Profarea  $profarea
     * @return mixed
     */
    public function update(User $user, Profarea $profarea)
    {
        return \Entrust::can('work.profareas.update');
    }

    /**
     * Determine whether the user can delete the profarea.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Profarea  $profarea
     * @return mixed
     */
    public function delete(User $user, Profarea $profarea)
    {
        return \Entrust::can('work.profareas.delete');
    }

    /**
     * Determine whether the user can restore the profarea.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Profarea  $profarea
     * @return mixed
     */
    public function restore(User $user, Profarea $profarea)
    {
        return \Entrust::can('work.profareas.delete');
    }

    /**
     * Determine whether the user can permanently delete the profarea.
     *
     * @param  \App\Models\Core\User  $user
     * @param  \App\Models\Work\Profarea  $profarea
     * @return mixed
     */
    public function forceDelete(User $user, Profarea $profarea)
    {
        return \Entrust::can('work.profareas.delete');
    }
}
