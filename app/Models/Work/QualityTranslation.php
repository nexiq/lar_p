<?php

namespace App\Models\Work;

use Illuminate\Database\Eloquent\Model;

class QualityTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'quality_id', 'locale'];
}
