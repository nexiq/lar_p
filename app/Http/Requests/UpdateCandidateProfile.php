<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCandidateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar_file' => ['sometimes', 'nullable'],
            'avatar_file.id' => ['sometimes', 'nullable', 'exists:files,id'],

            'first_name' => ['required', 'string', 'min:2', 'max:100',],
            'last_name' => ['required', 'string', 'min:2', 'max:100',],
            'middle_name' => ['sometimes', 'nullable', 'string',],
            'city_id' => ['required', 'exists:cities,id'],
            'dob' => ['required', 'date',],
            'sex' => ['required', 'boolean',],

            'qualities' => ['required', 'array', 'max:5',],
            'qualities.*' => ['exists:qualities,id'],
            'skills' => ['required', 'array', 'max:5',],
            'skills.*' => ['exists:skills,id'],

            'salary_monthly' => ['sometimes', 'nullable', 'integer', 'min:1'],
            'salary_daily' => ['sometimes', 'nullable', 'integer', 'min:1'],
            'salary_hourly' => ['sometimes', 'nullable', 'integer', 'min:1'],

            'work_type' => ['required', 'integer', 'min:1'],
            'work_schedule' => ['required', 'integer', 'min:1'],
            'work_team' => ['required', 'integer', 'min:1'],
            'work_relocation_ready' => ['required', 'boolean'],
            'relocation_city_id' => ['required_if:work_relocation_ready,1'],
            'trip_ready' => ['required', 'boolean'],
            'trip_city_id' => ['required_if:trip_ready,1'],

            'desired_profareas' => ['required', 'array',],
            'desired_profareas.*.profarea_id' => ['required', 'exists:profareas,id'],
            'desired_profareas.*.education_type' => ['required', 'integer'],
            'desired_profareas.*.experience_type' => ['required', 'integer'],

            'desired_specialities' => ['required', 'array',],
            'desired_specialities.*.speciality_id' => ['required', 'exists:specialities,id'],
            'desired_specialities.*.education_type' => ['required', 'integer'],
            'desired_specialities.*.experience_type' => ['required', 'integer'],

            'workplaces' => ['required', 'array',],
            'workplaces.*.profarea_id' => ['required', 'exists:profareas,id'],
            'workplaces.*.position_id' => ['required', 'exists:positions,id'],
            'workplaces.*.duration' => ['required', 'integer', 'min:1'],
            'workplaces.*.skills' => ['required', 'array', 'max:5'],
            'workplaces.*.skills.*' => ['exists:skills,id'],
            'workplaces.*.has_education' => ['required', 'boolean'],
            'workplaces.*.has_recommendation' => ['required', 'boolean'],

            'educations' => ['required', 'array',],
            'educations.*.type' => ['required', 'integer', 'min:1'],
            'educations.*.form' => ['required', 'integer', 'min:1'],
            'educations.*.place_name' => ['sometimes', 'nullable', 'string'],
            'educations.*.speciality' => ['required', 'exists:specialities,id'],
            'educations.*.graduate_year' => ['required', 'integer', 'min:1'],
            'educations.*.comment' => ['sometimes', 'nullable', 'string'],

            'height' => ['sometimes', 'nullable', 'integer'],
            'body_type' => ['sometimes', 'nullable', 'integer'],
            'clothing_size' => ['sometimes', 'nullable', 'integer'],
            'shoe_size' => ['sometimes', 'nullable', 'integer'],
            'is_smoking' => ['required', 'boolean',],
            'languages' => ['required', 'array', 'max:5'],
            'languages.*' => ['exists:languages,id'],
            'has_medical_book' => ['required', 'boolean',],
            'can_work_in_russia' => ['required', 'boolean',],
            'drivers_license' => ['sometimes', 'nullable', 'integer'],
        ];
    }
}
