<?php


namespace App\Http\Controllers\Moderation;

use App\Http\Controllers\Controller;
use App\Models\Profile\CandidateProfile;
use App\Models\Profile\ModerationAnswerTemplate;
use App\Services\Profile\CandidateService;
use App\Services\Profile\ModeratorStateService;
use App\Services\Profile\ProfileModerationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModerationController extends Controller
{
    private $service;

    public function __construct(CandidateService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $profile = CandidateProfile::find(ProfileModerationService::getCurrentProfileID());

        return view('moderation.index', [
            'profile' => $profile,
            'isWorking' => ModeratorStateService::isWorking(Auth::user()->id),
            'maxTime' => config('const.moderation.max_moderation_time') * 1000,
            'templates' => ModerationAnswerTemplate::all(),
        ]);
    }

    /**
     * Approve profile
     *
     * @param \App\Models\Profile\CandidateProfile $profile
     * @return \Illuminate\Http\Response
     */
    public function approve($id)
    {
        $profile = CandidateProfile::findOrFail($id);
        $this->service->approve($profile);
        return back();
    }

    /**
     * Decline profile
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function decline(Request $request)
    {
        $profile = CandidateProfile::findOrFail($request->input('id'));
        $this->service->decline($profile, $request->input('reason'));
        return back();
    }

    /**
     * Start work
     *
     * @return \Illuminate\Http\Response
     */
    public function start()
    {
        ModeratorStateService::startWork(Auth::user()->id);
        return back();
    }

    /**
     * Stop work
     *
     * @return \Illuminate\Http\Response
     */
    public function stop()
    {
        ModeratorStateService::stopWork(Auth::user()->id);
        return back();
    }
}
