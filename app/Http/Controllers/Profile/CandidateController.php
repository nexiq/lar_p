<?php


namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Profile\CandidateResource;
use App\Models\Profile\CandidateProfile;
use App\Services\Profile\CandidateService;

class CandidateController extends Controller
{
    private $service;

    public function __construct(CandidateService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $profiles = CandidateResource::collection(
            CandidateProfile::with([
                'city', 'city.translations',
                'tripCity', 'tripCity.translations',
                'relocationCity', 'relocationCity.translations',
                'skills', 'skills.translations',
                'qualities', 'qualities.translations',
                'languages', 'languages.translations',
                'desiredProfareas', 'desiredProfareas.profarea',
                'desiredProfareas.profarea.translations',
                'desiredSpecialities', 'desiredSpecialities.speciality',
                'desiredSpecialities.speciality.translations',
            ])->get()
        );
        return response()
            ->view(
                'profile.candidate.index',
                [
                    'profiles' => $profiles,
                ]
            );
    }

    public function view(CandidateProfile $profile)
    {
        return response()
            ->view(
                'profile.candidate.view',
                [
                    'profile' => new CandidateResource($profile),
                ]
            );
    }

    public function edit(CandidateProfile $profile)
    {
        $this->authorize('update', $profile);
        $data = $this->service->get($profile);
        return response()->view('profile.candidate.edit', [
            'profile' => new CandidateResource($data),
        ]);
    }
}
