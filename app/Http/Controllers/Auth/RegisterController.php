<?php

namespace App\Http\Controllers\Auth;

use App\Models\Core\User;
use App\Http\Controllers\Controller;
use App\Services\Core\RolesService;
use App\Services\Core\UsersService;
use App\Services\Profile\CandidateService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $registeredUser = null;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * @var UsersService
     */
    protected $usersService;

    /**
     * @var CandidateService
     */
    protected $candidateService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UsersService $usersService, CandidateService $candidateService)
    {
        $this->middleware('guest');
        $this->usersService = $usersService;
        $this->candidateService = $candidateService;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, $this->usersService->getValidationRules('create'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\Core\User
     */
    protected function create(array $data)
    {
        $availableRoles = config('roles.frontend');
        $targetRole = null;
        if (empty($data['role']) || !in_array($data['role'], $availableRoles)) {
            throw new \InvalidArgumentException("Role is not available");
        }
        $user = $this->usersService->create($data);

        $rolesService = new RolesService();

        $roles = $rolesService->getList([
            'name' => $data['role'],
        ])->toArray();

        $this->usersService->updateRoles($user, ['roles' => $roles]);

        if ($data['role'] == CandidateService::ASSIGN_ROLE) {
            $this->candidateService->createProfile($user);
        }
        return $user;
    }

    /**
     * Устанавливает юзера после регистрации, дабы потом определить куда его переадресовывать
     * @param Request $request
     * @param $user
     */
    protected function registered(Request $request, $user)
    {
        $this->registeredUser = $user;
    }

    /**
     * Метод определяетт переадресацию после регистрации
     * @return string
     */
    public function redirectTo()
    {
        return $this->usersService->getRouteAfterRegister($this->registeredUser) ?? $this->redirectTo;
    }
}
