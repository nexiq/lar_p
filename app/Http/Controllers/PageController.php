<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Content\Page;

class PageController extends Controller
{
    /**
     * Show the page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Page $page)
    {
        return response()->view('content.page', ['page' => $page]);
    }
}
