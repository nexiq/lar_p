<?php

namespace App\Http\Controllers\Api\Core;

use App\Models\Core\Role;
use App\Http\Resources\Core\RoleResource;
use App\Services\Core\RolesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * @var RolesService
     */
    protected $roleService = null;

    /**
     * Constucts Role Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Role::class, 'role');
        $this->roleService = new RolesService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Role::class);
        return RoleResource::collection($this->roleService->getList([]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        throw new \Exception("Создание новых ролей запрещено", 403);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Core\Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return new RoleResource($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Core\Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $role = $this->roleService->update($role, $request->all());
        return new RoleResource($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Core\Role $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        throw new \Exception("Удаление ролей запрещено", 403);
    }
}
