<?php

namespace App\Http\Controllers\Api\Core;

use App\Models\Core\User;
use App\Http\Resources\Core\UserResource;
use App\Services\Core\UsersService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;

/**
 * Class UserController
 * @package App\Http\Controllers\Api
 */
class UserController extends Controller
{

    /**
     * @var UsersService
     */
    protected $usersService = null;

    /**
     * Constucts User Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
        $this->usersService = new UsersService();
    }

    /**
     * Display the current user resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function me()
    {
        return new UserResource($this->usersService->getMe());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', User::class);
        return UserResource::collection($this->usersService->getList([]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newUser = $this->usersService->create($request->all());
        if ($request->has('roles')) {
            $this->usersService->updateRoles($newUser, $request->only('roles'));
        }
        return new UserResource($newUser);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Core\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Core\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user = $this->usersService->update($user, $request->all());
        if ($request->has('roles')) {
            $this->usersService->updateRoles($user, $request->only('roles'));
        }
        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Core\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        return $this->usersService->destroy($user);
    }
}
