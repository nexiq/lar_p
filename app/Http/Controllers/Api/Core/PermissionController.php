<?php

namespace App\Http\Controllers\Api\Core;

use App\Models\Core\Permission;
use App\Http\Resources\Core\PermissionResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    /**
     * Constucts Permission Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Permission::class, 'permission');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Permission::class);

        return PermissionResource::collection(Permission::all()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        throw new \Exception("Создание новых разрешений запрещено", 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Core\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        return new PermissionResource($permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Core\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        throw new \Exception("Редактирование разрешений запрещено", 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Core\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        throw new \Exception("Удаление разрешений запрещено", 403);
    }
}
