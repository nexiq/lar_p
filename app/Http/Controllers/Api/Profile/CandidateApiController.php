<?php

namespace App\Http\Controllers\Api\Profile;

use App\Http\Controllers\Controller;
use App\Http\Resources\Profile\CandidateResource;
use App\Http\Requests\UpdateCandidateProfile;
use App\Models\Profile\CandidateProfile;
use App\Models\Profile\Candidate\DesiredProfarea;
use App\Services\Profile\CandidateService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CandidateApiController extends Controller
{
    private $service;

    public function __construct(CandidateService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', CandidateProfile::class);
        return CandidateResource::collection(CandidateProfile::all()->keyBy->id);
    }

    /**
     * Display the specified resource.
     *
     * @param CandidateProfile $candidate
     * @return CandidateResource
     * @throws AuthorizationException
     */
    public function show(CandidateProfile $candidate)
    {
        $this->authorize('view', $candidate);
        return new CandidateResource($this->service->get($candidate));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCandidateProfile $request
     * @param CandidateProfile $candidate
     * @return CandidateResource
     * @throws AuthorizationException
     */
    public function update(
        UpdateCandidateProfile $request,
        CandidateProfile $candidate
    ) {
        $this->authorize('update', $candidate);

        $this->service->update($candidate, $request->validated());
        return new CandidateResource($this->service->get($candidate));
    }

    public function uploadAvatar(
        Request $request,
        CandidateProfile $candidate
    ) {
        $this->authorize('update', $candidate);
        return [
            'key' => 'avatar_file',
            'value' => $this->service->uploadAvatar($request->file),
        ];
    }
}
