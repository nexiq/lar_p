<?php

namespace App\Http\Controllers\Api\Work;

use App\Models\Work\Position;
use App\Http\Resources\Work\PositionResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Astrotomic\Translatable\Validation\RuleFactory;

class PositionController extends Controller
{
    /**
     * Constucts Position Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Position::class, 'position');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Position::class);

        return PositionResource::collection(Position::with('translations')->get()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $position = new Position();
        $position->fill($request->input('translations'));
        $position->save();
        return new PositionResource($position);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position)
    {
        return new PositionResource($position);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Position $position)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $position->fill($request->input('translations'));
        $position->save();
        return new PositionResource($position);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work\Position  $position
     * @return \Illuminate\Http\Response
     */
    public function destroy(Position $position)
    {
        $id = $position->id;
        $position->delete();
        return $id;
    }
}
