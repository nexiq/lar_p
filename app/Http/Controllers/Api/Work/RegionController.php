<?php

namespace App\Http\Controllers\Api\Work;

use App\Http\Controllers\Controller;
use App\Http\Resources\Work\RegionResource;
use App\Models\Work\Region;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * Constucts Region Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Region::class, 'region');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Region::class);

        return RegionResource::collection(Region::with('translations')->get()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'country_id' => ['sometimes', 'nullable', 'exists:countries,id'],
            'translations.%name%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $region = new Region();
        $region->fill($request->input('translations'));
        $region->country_id = $request->input('country_id');
        $region->save();
        return new RegionResource($region);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function show(Region $region)
    {
        return new RegionResource($region);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Region $region)
    {
        $rules = [
            'country_id' => ['sometimes', 'nullable', 'exists:countries,id'],
            'translations.%name%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $region->fill($request->input('translations'));
        $region->country_id = $request->input('country_id');
        $region->save();
        return new RegionResource($region);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work\Region  $region
     * @return \Illuminate\Http\Response
     */
    public function destroy(Region $region)
    {
        $id = $region->id;
        $region->delete();
        return $id;
    }
}
