<?php

namespace App\Http\Controllers\Api\Work;

use App\Models\Work\Skill;
use App\Http\Resources\Work\SkillResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Astrotomic\Translatable\Validation\RuleFactory;

class SkillController extends Controller
{
    /**
     * Constucts Skill Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Skill::class, 'skill');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Skill::class);

        return SkillResource::collection(Skill::with('translations')->get()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $skill = new Skill();
        $skill->fill($request->input('translations'));
        $skill->save();
        return new SkillResource($skill);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function show(Skill $skill)
    {
        return new SkillResource($skill);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Skill $skill)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $skill->fill($request->input('translations'));
        $skill->save();
        return new SkillResource($skill);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function destroy(Skill $skill)
    {
        $id = $skill->id;
        $skill->delete();
        return $id;
    }
}
