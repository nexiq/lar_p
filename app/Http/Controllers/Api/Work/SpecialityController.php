<?php

namespace App\Http\Controllers\Api\Work;

use App\Models\Work\Speciality;
use App\Http\Resources\Work\SpecialityResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Astrotomic\Translatable\Validation\RuleFactory;

class SpecialityController extends Controller
{
    /**
     * Constucts Speciality Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Speciality::class, 'speciality');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Speciality::class);

        return SpecialityResource::collection(Speciality::with('translations')->get()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $speciality = new Speciality();
        $speciality->fill($request->input('translations'));
        $speciality->save();
        return new SpecialityResource($speciality);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work\Speciality  $speciality
     * @return \Illuminate\Http\Response
     */
    public function show(Speciality $speciality)
    {
        return new SpecialityResource($speciality);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work\Speciality  $speciality
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Speciality $speciality)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $speciality->fill($request->input('translations'));
        $speciality->save();
        return new SpecialityResource($speciality);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work\Speciality  $speciality
     * @return \Illuminate\Http\Response
     */
    public function destroy(Speciality $speciality)
    {
        $id = $speciality->id;
        $speciality->delete();
        return $id;
    }
}
