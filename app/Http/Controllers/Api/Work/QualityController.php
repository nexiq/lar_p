<?php

namespace App\Http\Controllers\Api\Work;

use App\Models\Work\Quality;
use App\Http\Resources\Work\QualityResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Astrotomic\Translatable\Validation\RuleFactory;

class QualityController extends Controller
{
    /**
     * Constucts Quality Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Quality::class, 'quality');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Quality::class);

        return QualityResource::collection(Quality::with('translations')->get()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $quality = new Quality();
        $quality->fill($request->input('translations'));
        $quality->save();
        return new QualityResource($quality);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work\Quality  $quality
     * @return \Illuminate\Http\Response
     */
    public function show(Quality $quality)
    {
        return new QualityResource($quality);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work\Quality  $quality
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quality $quality)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $quality->fill($request->input('translations'));
        $quality->save();
        return new QualityResource($quality);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work\Quality  $quality
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quality $quality)
    {
        $id = $quality->id;
        $quality->delete();
        return $id;
    }
}
