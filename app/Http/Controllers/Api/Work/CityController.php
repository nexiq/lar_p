<?php

namespace App\Http\Controllers\Api\Work;

use App\Models\Work\City;
use App\Http\Resources\Work\CityResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Astrotomic\Translatable\Validation\RuleFactory;

class CityController extends Controller
{
    /**
     * Constucts City Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(City::class, 'city');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', City::class);

        return CityResource::collection(City::with('translations')->get()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'region_id' => ['sometimes', 'nullable', 'exists:regions,id'],
            'translations.%name%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $city = new City();
        $city->fill($request->input('translations'));
        $city->region_id = $request->input('region_id');
        $city->save();
        return new CityResource($city);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        return new CityResource($city);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $rules = [
            'region_id' => ['sometimes', 'nullable', 'exists:regions,id'],
            'translations.%name%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $city->fill($request->input('translations'));
        $city->region_id = $request->input('region_id');
        $city->save();
        return new CityResource($city);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $id = $city->id;
        $city->delete();
        return $id;
    }
}
