<?php

namespace App\Http\Controllers\Api\Work;

use App\Models\Work\Language;
use App\Http\Resources\Work\LanguageResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Astrotomic\Translatable\Validation\RuleFactory;

class LanguageController extends Controller
{
    /**
     * Constucts Language Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Language::class, 'language');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Language::class);

        return LanguageResource::collection(Language::with('translations')->get()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $language = new Language();
        $language->fill($request->input('translations'));
        $language->save();
        return new LanguageResource($language);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        return new LanguageResource($language);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $language)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $language->fill($request->input('translations'));
        $language->save();
        return new LanguageResource($language);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        $id = $language->id;
        $language->delete();
        return $id;
    }
}
