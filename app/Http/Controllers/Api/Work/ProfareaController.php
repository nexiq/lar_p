<?php

namespace App\Http\Controllers\Api\Work;

use App\Models\Work\Profarea;
use App\Http\Resources\Work\ProfareaResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Astrotomic\Translatable\Validation\RuleFactory;

class ProfareaController extends Controller
{
    /**
     * Constucts Profarea Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Profarea::class, 'profarea');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Profarea::class);

        return ProfareaResource::collection(Profarea::with('translations')->get()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $profarea = new Profarea();
        $profarea->fill($request->input('translations'));
        $profarea->save();
        return new ProfareaResource($profarea);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work\Profarea  $profarea
     * @return \Illuminate\Http\Response
     */
    public function show(Profarea $profarea)
    {
        return new ProfareaResource($profarea);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work\Profarea  $profarea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profarea $profarea)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
        ];
        $request->validate(RuleFactory::make($rules));

        $profarea->fill($request->input('translations'));
        $profarea->save();
        return new ProfareaResource($profarea);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work\Profarea  $profarea
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profarea $profarea)
    {
        $id = $profarea->id;
        $profarea->delete();
        return $id;
    }
}
