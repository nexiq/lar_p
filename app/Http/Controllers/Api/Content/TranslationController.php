<?php

namespace App\Http\Controllers\Api\Content;

use App\Http\Resources\Content\TranslationResource;
use App\Models\Content\Translation;
use App\Services\Content\TranslationService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TranslationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ResourceCollection
     */
    public function index(): ResourceCollection
    {
        $this->authorize('viewAny', Translation::class);
        return TranslationResource::collection(
            collect(
                (new TranslationService())->getTranslations()
            )
                ->map(function ($translations, $locale) {
                    return [
                        'locale' => $locale,
                        'messages' => $translations,
                    ];
                })
                ->values()
        );
    }

    /**
     * Display the specified resource.
     *
     * @param string $locale Двузначный код локали
     * @return TranslationResource
     */
    public function show($locale): TranslationResource
    {
        $this->authorize('view', Translation::class);
        return new TranslationResource([
            'locale' => $locale,
            'messages' => (new TranslationService())->getTranslation($locale),
        ]);
    }

    /**
     *  Сохраняет перевод для локали
     *
     * @param \Illuminate\Http\Request $request
     * @return TranslationResource
     */
    public function update(Request $request, $locale): TranslationResource
    {
        $this->authorize('update', Translation::class);

        $request->validate([
            'messages' => ['required', 'array'],
        ]);
        $translationService = new TranslationService();
        return new TranslationResource($translationService->storeTranslation($locale, $request->post('messages')));
    }
}
