<?php

namespace App\Http\Controllers\Api\Content;

use App\Models\Content\Page;
use App\Http\Resources\Content\PageResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Astrotomic\Translatable\Validation\RuleFactory;

class PageController extends Controller
{
    /**
     * Constucts Page Api Controller
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Page::class, 'page');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Page::class);

        return PageResource::collection(Page::all()->keyBy->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'slug' => ['required', 'string', 'max:100', 'unique:pages'],
            'translations.%title%' => ['required', 'string', 'max:100'],
            'translations.%description%' => ['required', 'string'],
            'translations.%content%' => ['required', 'string'],
        ];
        $request->validate(RuleFactory::make($rules));

        $page = new Page();
        $page->fill($request->input('translations'));
        $page->slug = $request->input('slug');
        $page->save();
        return new PageResource($page);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Content\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return new PageResource($page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Content\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $rules = [
            'translations.%title%' => ['required', 'string', 'max:100'],
            'translations.%description%' => ['required', 'string'],
            'translations.%content%' => ['required', 'string'],
        ];
        $request->validate(RuleFactory::make($rules));

        $page->fill($request->input('translations'));
        $page->save();
        return new PageResource($page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Content\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $id = $page->id;
        $page->delete();
        return $id;
    }
}
