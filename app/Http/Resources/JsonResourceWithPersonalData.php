<?php


namespace App\Http\Resources;

use App\Models\Core\HasPersonalDataInterface;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Http\Resources\Json\JsonResource;

class JsonResourceWithPersonalData extends JsonResource
{
    public function __construct($resource)
    {
        if ($resource instanceof HasPersonalDataInterface) {
            if (!\Entrust::can('core.personal.read')) {
                $resource->hidePersonalDataAttributes();
            }
        }
        parent::__construct($resource);
    }
}
