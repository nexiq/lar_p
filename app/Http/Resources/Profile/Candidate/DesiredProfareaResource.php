<?php

namespace App\Http\Resources\Profile\Candidate;

use App\Http\Resources\JsonResourceWithPersonalData;

class DesiredProfareaResource extends JsonResourceWithPersonalData
{
    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'education_type' => $this->education_type,
            'experience_type' => $this->experience_type,
            'profarea' => $this->profarea,
        ];
    }
}
