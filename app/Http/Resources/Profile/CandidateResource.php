<?php

namespace App\Http\Resources\Profile;

use App\Http\Resources\JsonResourceWithPersonalData;
use App\Http\Resources\Profile\Candidate\DesiredProfareaResource;

class CandidateResource extends JsonResourceWithPersonalData
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'middle_name' => $this->middle_name,
            'dob' => $this->dob,
            'sex' => $this->sex,
            'is_smoking' => $this->is_smoking,
            'has_medical_book' => $this->has_medical_book,
            'can_work_in_russia' => $this->can_work_in_russia,

            'height' => $this->height,
            'weight' => $this->weight,
            'clothing_size' => $this->clothing_size,
            'shoe_size' => $this->shoe_size,
            'body_type' => $this->body_type,

            'city_id' => $this->city_id,

            'salary_monthly' => $this->salary_monthly,
            'salary_daily' => $this->salary_daily,
            'salary_hourly' => $this->salary_hourly,
            'work_type' => $this->work_type,
            'work_schedule' => $this->work_schedule,
            'work_team' => $this->work_team,

            'work_relocation_ready' => $this->work_relocation_ready,
            'relocation_city_id' => $this->relocation_city_id,

            'desired_profareas' => $this->desiredProfareas,
            'desired_specialities' => $this->desiredSpecialities,
            'workplaces' => $this->workplaces,
            'educations' => $this->educations,
            'languages' => $this->languages->modelKeys(),
            'qualities' => $this->qualities->modelKeys(),
            'skills' => $this->skills->modelKeys(),

            'avatar_file' => $this->avatarFile,

            'trip_ready' => $this->trip_ready,
            'trip_city_id' => $this->trip_city_id,
            'drivers_license' => $this->drivers_license,
        ];
    }
}
