<?php

namespace App\Http\Resources\Core;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Core\PermissionResource;

class RoleResource extends JsonResource
{
    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'display_name' => $this->display_name,
            'description' => $this->description,
            'permissions' => PermissionResource::collection($this->perms->keyBy->id)
        ];
    }
}
