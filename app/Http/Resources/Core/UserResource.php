<?php

namespace App\Http\Resources\Core;

use App\Http\Resources\JsonResourceWithPersonalData;

class UserResource extends JsonResourceWithPersonalData
{
    /**
     * Indicates if the resource's collection keys should be preserved.
     *
     * @var bool
     */
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'roles' => RoleResource::collection($this->roles->keyBy->id),
            'role_ids' => $this->roles->modelKeys(),
            'phone' => $this->phone,
            'phone_ext' => $this->phone_ext,
        ];
    }
}
