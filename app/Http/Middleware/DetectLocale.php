<?php

namespace App\Http\Middleware;

use Closure;

class DetectLocale
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Detect locale if present
        if ($request->lang) {
            app()->setLocale($request->lang);
        }

        if ($request->headers->has('X-LANG')) {
            app()->setLocale($request->header('X-LANG'));
        }

        // Set current locale as default value for URL generator
        $default = app()->getLocale() == 'ru' ? null : app()->getLocale();

        if ($default === null && $request->user() && $request->user()->locale) {
            $default = $request->user()->locale;
            app()->setLocale($default);
        }
        url()->defaults(['lang' => $default]);

        $request->route()->forgetParameter('lang');

        return $next($request);
    }
}
