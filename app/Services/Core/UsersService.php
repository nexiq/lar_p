<?php

namespace App\Services\Core;

use App\Events\Core\UserUpdatedEvent;
use App\Models\Core\User;
use App\Services\AbstractService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

/**
 * Class UsersService
 * @package App\Services\Core
 */
class UsersService extends AbstractService
{

    /**
     * Правила валидацтт
     * @var array
     */
    protected $validationRules = [

        // правила валидации для создания из админки
        'create' => [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'phone', 'unique:users',],
            'phone_ext' => ['sometimes', 'nullable', 'digits_between:1,20'],
        ],
        'update' => [
            'password' => ['string', 'min:8', 'confirmed'],
        ],
        'roles' => [
            'roles' => ['array',]
        ],
    ];

    /**
     * Генерирует апи ключ (вскоре будет @return string
     * @deprecated )
     */
    protected function createApiKey()
    {
        return Str::random(60);
    }

    /**
     * Создает пользователя
     * @param $data
     * @return User
     */
    public function create($data = [])
    {
        $validated = Validator::make($data, $this->validationRules['create'])->validate();
        $justCreatedUser = User::create($validated);
        $justCreatedUser->api_token = $this->createApiKey();
        $justCreatedUser->locale = $data['locale'] ?? app()->getLocale();

        // Стандартную не пишем в бд
        if ($justCreatedUser->locale == config('app.locale')) {
            $justCreatedUser->locale = null;
        }
        $this->setNewPassword($justCreatedUser, $validated['password']);

        event(new Registered($justCreatedUser));

        return $justCreatedUser;
    }

    /**
     * Выставляет роли
     * @param User $user
     * @param $data
     * @return $this
     */
    public function updateRoles(User $user, $data)
    {
        $validated = Validator::make($data, $this->validationRules['roles'])->validate();
        $rolesIds = array_keys($validated['roles']);
        $this->syncRoles($user, $rolesIds);
        return $this;
    }

    /**
     * Получает список пользователей согласно фильтрам
     * @param array $filters - критерии отбора
     * @return Collection
     */

    public function getList($filters = [])
    {
        return User
            ::with([
                'roles',
            ])
            ->get()
            ->keyBy->id;
    }

    /**
     * Возвращает "себя" как пользователя
     * @return User|null
     */
    public function getMe()
    {
        return $this->getUser();
    }

    /**
     * Добавляет роль к пользователю
     * @param mixed $roleId - ИД роли или массив ролей
     * @return UsersService
     */
    public function syncRoles(User $user, $roleId)
    {
        if (!is_array($roleId)) {
            $roleId = [$roleId];
        }
        $user->roles()->sync($roleId);
        return $this;
    }

    /**
     * Обновляет пользователя
     * @param User $targetUser
     * @param array $data
     * @return User
     */
    public function update(User $targetUser, $data = [])
    {
        $validated = Validator::make($data, $this->validationRules['update'])->validate();
        if (!empty($validated['password'])) {
            $this->setNewPassword($targetUser, $validated['password']);
        }
        $targetUser->save();
        if (isset($validated['roles'])) {
            $this->syncRoles($targetUser, array_keys($validated['roles']));
        }
        event(new UserUpdatedEvent($targetUser));
        return $targetUser;
    }

    /**
     * Обнолвяет пароль
     * @param User $targetUser
     * @param $newPassword
     * @return $this
     */
    public function setNewPassword(User $targetUser, $newPassword)
    {
        $targetUser->password = $this->hashPassword($newPassword);
        $targetUser->save();
        return $this;
    }

    /**
     * Хеширует пароль
     * @param $password
     * @return string
     */
    protected function hashPassword($password)
    {
        return Hash::make($password);
    }

    public function destroy(User $user)
    {
        $id = $user->id;
        $user->delete();
        return $id;
    }

    /**
     * Возвращает редирект страницы после регистрации
     * @param User $user
     */
    public function getRouteAfterRegister(User $user)
    {
        $userRoles = $user->roles()->pluck('name')->toArray();
        foreach (config('roles.registrationRedirect') as $roleName => $routeName) {
            if (!in_array($roleName, $userRoles)) {
                continue;
            }
            if (is_callable($routeName)) {
                return $routeName($user);
            }
            return route($routeName, [
                'lang' => $user->locale
            ]);
        }
        // fallback
        return null;
    }

    /**
     * Возвращает УРЛ домашней страницы
     * @param User $user
     */
    public function getHomeRoute(User $user)
    {
        $userRoles = $user->roles()->pluck('name')->toArray();
        foreach (config('roles.homeRedirect') as $roleName => $routeName) {
            if (!in_array($roleName, $userRoles)) {
                continue;
            }
            if (is_callable($routeName)) {
                return $routeName($user);
            }
            return route($routeName, [
                'lang' => $user->locale,
            ]);
        }
        // fallback
        return null;
    }
}
