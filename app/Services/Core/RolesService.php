<?php

namespace App\Services\Core;

use App\Models\Core\Role;
use App\Services\AbstractService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class RolesService extends AbstractService
{
    protected $validationRules = [
        'update' => [
            'display_name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'permissions' => ['array']
        ]
    ];

    /**
     * Обновляет роль
     * @param Role $role
     * @param array $data
     * @return Role
     */
    public function update(Role $role, $data = [])
    {
        $validated = Validator::make($data, $this->validationRules['update'])->validate();
        $role->fill(Arr::only($validated, [
            'display_name',
            'description',
        ]));
        $role->save();

        if (isset($validated['permissions'])) {
            $this->syncPermissions($role, array_keys($validated['permissions']));
        }
        return $role;
    }

    /**
     * Синхронизирует права
     * @param Role $role
     * @param array $permissions
     */
    public function syncPermissions(Role $role, $permissions = [])
    {
        $role->perms()->sync($permissions);
        return $this;
    }

    /**
     * Возвращает список ролей согласно фильтру (если надо)
     * @param array $filters
     * @return Collection
     */
    public function getList($filters = [])
    {
        return Role
            ::with('perms')
            ->when(!empty($filters['name']), function ($q) use ($filters) {
                $q->where('name', $filters['name']);
            })
            ->get()
            ->keyBy->id;
    }
}
