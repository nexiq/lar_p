<?php

namespace App\Services\Profile;

use App\Models\Core\User;
use App\Models\Profile\CandidateProfile;
use App\Models\Profile\ProfileModeration;
use App\Services\AbstractService;
use App\Services\Content\UploadService\ImageUploadService;
use App\Services\Content\UploadService\UploadService;
use Illuminate\Http\UploadedFile;

class CandidateService extends AbstractService
{
    const ASSIGN_ROLE = 'candidate';

    /**
     * @var UploadService
     */
    private $uploadService;

    /**
     * @var ImageUploadService
     */
    private $imageUploadService;

    public function __construct(
        UploadService $uploadService,
        ImageUploadService $imageUploadService
    ) {
        $this->uploadService = $uploadService;
        $this->imageUploadService = $imageUploadService;
    }

    /**
     * Создает профайл
     * @param User $user
     * @return CandidateProfile
     */
    public function createProfile(User $user)
    {
        $profile = CandidateProfile::where(['user_id' => $user->id])->first();
        if ($profile) {
            return $profile;
        }
        $profile = CandidateProfile::create([
            'first_name' => $user->name,
            'user_id' => $user->id,
        ]);
        return $profile;
    }

    /**
     * Eager profile loading
     *
     * @param CandidateProfile $profile
     * @return CandidateProfile
     */
    public function get(CandidateProfile $profile)
    {
        $profile->load([
            'resumeFile',
            'avatarFile',
            'skills',
            'qualities',
            'desiredProfareas',
            'desiredSpecialities',
            'workplaces',
            'educations',
            'languages',
        ]);
        return $profile;
    }

    /**
     * Обновляет профайл
     * @param CandidateProfile $currentProfile
     * @param array $data
     * @return CandidateProfile
     * @throws \Exception
     */
    public function update(CandidateProfile $currentProfile, array $data)
    {
        // Remove old avatar if needed
        if ($currentProfile->avatarFile
            && $currentProfile->avatarFile->id != $data['avatar_file']['id']
        ) {
            $this->imageUploadService->destroy($currentProfile->avatarFile);
        }
        // Update avatar reference
        $currentProfile->avatar_file_id = $data['avatar_file']['id'];

        // Sync relations
        $currentProfile->qualities()->sync($data['qualities']);
        $currentProfile->skills()->sync($data['skills']);
        $currentProfile->languages()->sync($data['languages']);

        // Update complex relations
        $this->updateDesiredProfareas($currentProfile, $data['desired_profareas']);
        $this->updateDesiredSpecialities($currentProfile, $data['desired_specialities']);

        // Fill profile with validated data
        $currentProfile->fill($data);
        $currentProfile->save();
        return $currentProfile;
    }

    public function updateDesiredProfareas(
        CandidateProfile $currentProfile,
        array $desiredProfareas
    ) {
        if (count($desiredProfareas)) {
            foreach ($desiredProfareas as $desiredProfarea) {
                $currentProfile->desiredProfareas()->updateOrCreate(
                    ['profarea_id' => $desiredProfarea['profarea_id']],
                    [
                        'experience_type' => $desiredProfarea['experience_type'],
                        'education_type' => $desiredProfarea['education_type'],
                    ]
                );
            }
            // Delete unused records
            $currentProfile->desiredProfareas()->whereNotIn(
                'profarea_id',
                collect($desiredProfareas)->pluck('profarea_id')->all()
            )->delete();
        }
    }

    public function updateDesiredSpecialities(
        CandidateProfile $currentProfile,
        array $desiredSpecialities
    ) {
        if (count($desiredSpecialities)) {
            foreach ($desiredSpecialities as $desiredSpeciality) {
                $currentProfile->desiredSpecialities()->updateOrCreate(
                    ['speciality_id' => $desiredSpeciality['speciality_id']],
                    [
                        'experience_type' => $desiredSpeciality['experience_type'],
                        'education_type' => $desiredSpeciality['education_type'],
                    ]
                );
            }
            // Delete unused records
            $currentProfile->desiredSpecialities()->whereNotIn(
                'speciality_id',
                collect($desiredSpecialities)->pluck('speciality_id')->all()
            )->delete();
        }
    }

    public function uploadAvatar(UploadedFile $file)
    {
        $newFile = $this->imageUploadService->storeImage($file, 'avatars', true);
        $this->imageUploadService->registerUploadTry(
            CandidateProfile::class,
            $newFile,
            'avatar_file_id'
        );
        return $newFile;
    }

    /**
     * Подтвердить анкету
     * @param CandidateProfile $profile
     */

    public function approve(CandidateProfile $profile)
    {
        $profile->status = config('const.profile.candidate.status.approved');
        $profile->save();
        if ($profile->moderation) {
            $profile->moderation->delete();
        }
    }

    /**
     * Отклонить анкету
     * @param CandidateProfile $profile
     */

    public function decline(CandidateProfile $profile, $reason)
    {
        $profile->status = config('const.profile.candidate.status.declined');
        $profile->save();
        if ($profile->moderation) {
            $profile->moderation->reason = $reason;
            $profile->moderation->save();
        }
    }
}
