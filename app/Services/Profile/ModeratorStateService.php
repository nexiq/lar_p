<?php

namespace App\Services\Profile;

use App\Models\Profile\ModeratorState;
use App\Services\AbstractService;

/**
 * Class ModeratorStateService
 * @package App\Services\Profile
 */
class ModeratorStateService extends AbstractService
{
    /**
     * Проверка работает ли модератор
     * @return boolean
     */
    public static function isWorking($user_id)
    {
        $is_exists = ModeratorState::where('user_id', $user_id)
            ->whereNull('stop_at')
            ->exists();

        return $is_exists;
    }

    /**
     * Начать работу
     */
    public static function startWork($user_id)
    {
        if (!self::isWorking($user_id)) {
            ModeratorState::create([
                'user_id' => $user_id,
                'start_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

    /**
     * Закончить работу
     */
    public static function stopWork($user_id)
    {
        ModeratorState::where('user_id', '=', $user_id)
            ->whereNull('stop_at')
            ->update(['stop_at' => date('Y-m-d H:i:s')]);
    }
}
