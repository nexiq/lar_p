<?php

namespace App\Services\Profile;

use App\Models\Core\User;
use App\Models\Profile\CandidateProfile;
use App\Models\Profile\ModeratorState;
use App\Models\Profile\ProfileModeration;
use App\Services\AbstractService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class ProfileModerationService
 * @package App\Services\Profile
 */
class ProfileModerationService extends AbstractService
{
    /**
     * Получает свободную для модерации анкету
     * @return integer|null
     */
    public static function getFreeProfileID()
    {
        $result = DB::table('candidate_profiles')
            ->leftJoin('profile_moderations', 'profile_moderations.candidate_profile_id', '=', 'candidate_profiles.id')
            ->select('candidate_profiles.*')
            ->whereNull('profile_moderations.user_id')
            ->where('candidate_profiles.status', '=', 1)
            ->first();
        if ($result) {
            ProfileModeration::create([
                'candidate_profile_id' => $result->id,
                'user_id' => Auth::user()->id,
                'deadline_at' => date('Y-m-d H:m:s', strtotime('now + 10 minutes'))
            ]);
        }

        return $result ? $result->id : null;
    }

    /**
     * Получает текущую для модерации анкету
     * @return integer|null
     */
    public static function getCurrentProfileID()
    {
        $moderation = ProfileModeration::where('user_id', Auth::user()->id)
            ->whereNull('reason')->with('profile')->first();
        $id = $moderation ? $moderation->profile->id : self::getFreeProfileID();
        return $id;
    }

    /**
     * Удаляет неактивные модерации
     */
    public static function updateInactiveProfiles()
    {
        ProfileModeration::where('deadline_at', '<', date('Y-m-d H:i:s'))
            ->delete();
    }
}
