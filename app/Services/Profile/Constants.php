<?php


namespace App\Services\Profile;

class Constants
{
    /*
     * Пол
     */
    const SEX_MALE = 0;
    const SEX_FEMALE = 1;

    /**
     * Курит ли
     */
    const IS_SMOKING_YES = 1;
    const IS_SMOKING_NO = 0;

    /**
     * Наличие мед книги
     */
    const MEDICAL_BOOK_YES = 1;
    const MEDICAL_BOOK_NO = 0;

    /**
     * Право работать в РФ
     */
    const CAN_WORK_IN_RUSSIA_YES = 1;
    const CAN_WORK_IN_RUSSIA_NO = 0;


    /**
     * Тип графика работы
     */
    const WORK_TYPE_REGULAR = 0b1;
    const WORK_TYPE_TEMP = 0b10;
    const WORK_TYPE_PROJECT = 0b100;
    const WORK_TYPE_HOURLY = 0b1000;
    const WORK_TYPE_ONETIME = 0b10000;
    const WORK_TYPE_REMOTE = 0b100000;

    /**
     * График работы
     */
    const WORK_SCHEDULE_52 = 0b1;
    const WORK_SCHEDULE_22 = 0b10;
    const WORK_SCHEDULE_WEEKEND = 0b100;

    /**
     * Атмосфера в коллективе
     */
    const WORK_TEAM_POSITIVE = 0b1;
    const WORK_TEAM_NEGATIVE = 0b10;
    const WORK_TEAM_NEUTRAL = 0b100;
    const WORK_TEAM_FRIDAY = 0b1000;

    /*
     * Статусы профилей кандидатов
     * Отрицательные - ошибка или что-то не так
     * Положительные - нормальные проверенные анкеты
     */
    const CANDIDATE_PROFILE_STATUS_UNVERIFIED = 0;
    const CANDIDATE_PROFILE_STATUS_ERROR = -1;
    const CANDIDATE_PROFILE_STATUS_APPROVED = 1;

    /**
     * Образование по профобласти
     */
    const PROFAREA_SPECICAL_EDUCATION = 0b1;
    const PROFAREA_HIGHER_EDUCATION = 0b10;
    const PROFAREA_SECONDARY_EDUCATION = 0b100;
    const PROFAREA_PROFESSIONAL_EDUCATION = 0b1000;
    const PROFAREA_STUDENT = 0b10000;

    /**
     * Опыт по профобласти
     */

    const PROFAREA_EXPERIENCE_NO = 0b1;
    const PROFAREA_EXPERIENCE_3M = 0b10;
    const PROFAREA_EXPERIENCE_1Y = 0b100;
    const PROFAREA_EXPERIENCE_3Y = 0b1000;
    const PROFAREA_EXPERIENCE_MORE_3Y = 0b10000;
}
