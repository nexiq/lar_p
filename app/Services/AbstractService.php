<?php

namespace App\Services;

use App\Models\Core\User;

abstract class AbstractService
{
    /**
     * @var User
     */
    protected $user = null;

    /**
     * Правила валидацтт
     * @var array
     */
    protected $validationRules = [
    ];

    public function setUser(User $contextUser): self
    {
        $this->user = $contextUser;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user ?? auth()->user();
    }

    /**
     * Возвращает правила валидации по имени
     * @param $name
     * @return mixed|null
     */
    public function getValidationRules($name)
    {
        return $this->validationRules[$name] ?? null;
    }
}
