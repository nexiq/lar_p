<?php

namespace App\Services\Content\UploadService;

use App\Models\Content\UploadTry;
use App\Models\Core\User;
use App\Services\AbstractService;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use App\Models\Content\File as ContentFile;

class UploadService extends AbstractService implements UploadServiceInterface
{
    const EXCEPTION_BAD_UPLOAD = 1;
    const EXCEPTION_BAD_UPLOAD_EXTENSION = 2;

    protected $exceptionDescriptions = [
        self::EXCEPTION_BAD_UPLOAD_EXTENSION => 'Bad extension %s',
        self::EXCEPTION_BAD_UPLOAD => 'Some errors with uploaded file',
    ];
    /**
     * @var Filesystem
     */
    protected $diskName = null;

    /**
     * Список запрещенных разрешений при которых загрузка неполучится
     * @var array
     */
    protected $strictExtensions = [
        'php',
        'exe',
        'html',
        'htm',
    ];

    /**
     * Список разрешенных расширений. Если не пуст - проверяет по нему, если не попало - ошибка
     * @var array
     */
    protected $validExtension = [

    ];

    /**
     *
     * @see https://laravel.com/docs/5.8/filesystem
     * @param string $diskName Название диска куда будет сохранять файлы сервис. Перечень дисков config.filesystems
     */
    public function __construct(string $diskName = null)
    {
        if (!$diskName) {
            $diskName = config('filesystems.default');
        }
        $this->setDiskName($diskName);
    }

    /**
     * Выставляет переводы для ошибок
     * @param array $exceptionDescriptions
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setExceptionDescriptions(array $exceptionDescriptions = [])
    {
        if (empty($exceptionDescriptions)) {
            throw new \InvalidArgumentException("exceptionDescriptions argument must be not empty");
        }
        $this->exceptionDescriptions = $exceptionDescriptions;
        return $this;
    }

    /**
     * Выставляет диск
     * @param $diskName
     * @return $this
     */
    public function setDiskName(string $diskName)
    {
        $this->diskName = $diskName;
        return $this;
    }

    /**
     * Получает текущий диск
     * @return string
     */
    public function getDiskName()
    {
        return $this->diskName;
    }

    /**
     * Сохраняет файл в диск и возвращает объект Content\File в случае успеха
     * @param UploadedFile $file
     * @param string $directoryPrefix Префикс папки куда сохранять. К примеру avatars, news, resume, etc.
     * @return ContentFile
     */
    public function storeFile(UploadedFile $file, $directoryPrefix = null): ContentFile
    {
        if ($this->getUser()->cannot('content.files.create')) {
            throw new UnauthorizedException;
        }
        if (!$this->isValidExtension($file->getClientOriginalExtension())) {
            throw new UploadBadExtensionException(
                sprintf(
                    $this->exceptionDescriptions[self::EXCEPTION_BAD_UPLOAD_EXTENSION],
                    $file->getClientOriginalExtension()
                )
            );
        }
        $hashName = $file->hashName();
        $targetSubDirectory = $hashName[0] . DIRECTORY_SEPARATOR . $hashName[1] . DIRECTORY_SEPARATOR;

        $resultDestination = ($directoryPrefix ? ($directoryPrefix . DIRECTORY_SEPARATOR) : '');
        $resultDestination .= $targetSubDirectory . $hashName;
        $putSuccessful = $this->put($resultDestination, $file);
        if (!$putSuccessful) {
            throw new UploadException(
                $this->exceptionDescriptions[self::EXCEPTION_BAD_UPLOAD],
                self::EXCEPTION_BAD_UPLOAD
            );
        }


        $contentFile = new ContentFile();
        $contentFile->name = $file->getClientOriginalName();
        $contentFile->extension = $file->getClientOriginalExtension();
        $contentFile->size = $file->getSize();
        $contentFile->user_id = $this->getUser()->id;
        $contentFile->disk = $this->diskName;
        $contentFile->local_path = $resultDestination;
        $contentFile->url = Storage::disk($this->diskName)->url($resultDestination);
        $contentFile->save();
        return $contentFile;
    }

    public function put($destination, $file)
    {
        return Storage::disk($this->diskName)->put($destination, file_get_contents($file));
    }

    /**
     * Проверяет на валидность
     * @param $extension
     * @return bool
     */
    protected function isValidExtension($extension)
    {
        if (empty($extension)) {
            return false;
        }
        if (!empty($this->validExtension)) {
            return in_array($extension, $this->validExtension);
        }
        return !in_array(strtolower($extension), $this->strictExtensions);
    }

    /**
     * Удаляет сущность файла как в бд так и физически
     * @param ContentFile $file
     * @return self
     * @throws \Exception
     */
    public function destroy(ContentFile $file)
    {
        if ($this->getUser()->cannot('content.files.delete')) {
            throw new UnauthorizedException;
        }
        Storage::disk($file->disk)->delete($file->local_path ?? $file->url);
        $file->delete();
        return $this;
    }

    /**
     * Создает колонку file_id и связывает внешним ключем
     * @param Blueprint $table
     */
    public static function uploadFileColumn(Blueprint $table, $defaultField = 'file_id')
    {
        $table->bigInteger($defaultField)->nullable();
        $table->foreign($defaultField)->references('id')->on('files')->onDelete('set null');
    }

    /**
     * Удаляет из структуры таблицы ключ и поле file_id
     * @param Blueprint $table
     */
    public static function dropUploadFileColumn(Blueprint $table, $defaultField = 'file_id')
    {
        $table->dropForeign(implode('_', [
            strtolower($table->getTable()),
            $defaultField,
            'foreign',
        ]));
        $table->dropColumn($defaultField);
    }

    /**
     * Сохраняет попытку загрузки файлов
     * @param $model тип модели. Нужно передать название как Model::class, либо же передать инстанс модели
     * @param ContentFile $file
     * @param string $field
     * @return UploadTry
     */
    public function registerUploadTry($model, ContentFile $file, $field = 'file_id')
    {
        if ($model instanceof Model) {
            $model = get_class($model);
        }
        if (!is_string($model)) {
            throw new \InvalidArgumentException("Bad model argument");
        }

        $uploadTry = new UploadTry();
        $uploadTry->fill([
            'model' => $model,
            'field' => $field,
            'file_id' => $file->id,
        ]);
        $uploadTry->save();
        return $uploadTry;
    }

    /**
     * Очищает мертвые файлы
     * @param null $userEmail
     */
    public static function cleanUploadTries($userEmail = null)
    {
        if (empty($userEmail)) {
            throw new \InvalidArgumentException("userEmail is empty");
        }
        $user = User::where('email', $userEmail)->first();
        if (!$user) {
            throw new \InvalidArgumentException("User with email [" . $userEmail . "] not found");
        }

        $uploadService = (new self())->setUser($user);
        UploadTry::with('file')
            ->recent()
            ->chunk(100, function ($uploadTries) use ($uploadService) {
                foreach ($uploadTries as $uploadTry) {
                    $modelType = $uploadTry->model;
                    try {
                        $builder = call_user_func($modelType . '::where', [
                            $uploadTry->field => $uploadTry->file->id,
                        ]);
                        if (!$builder instanceof Builder) {
                            throw new \Exception();// some wrong with try
                        }
                        $model = $builder->first();
                        if (!$model) {
                            $uploadService->destroy($uploadTry->file);
                        }
                    } catch (\Error $err) {
                    } catch (\Exception $exception) {
                    } finally {
                        $uploadTry->delete();
                    }
                }
            });
    }
}
