<?php

namespace App\Services\Content\UploadService;

use App\Models\Content\UploadTry;
use App\Models\Content\File as ContentFile;
use Illuminate\Http\UploadedFile;

interface UploadServiceInterface
{
    /**
     * Сохраняет файл в диск и возвращает объект Content\File в случае успеха
     * @param UploadedFile $file
     * @param string $directoryPrefix Префикс папки куда сохранять. К примеру avatars, news, resume, etc.
     * @return ContentFile
     */
    public function storeFile(UploadedFile $file, $directoryPrefix = null): ContentFile;


    /**
     * Удаляет сущность файла как в бд так и физически
     * @param \App\Models\Content\File $file
     * @return self
     */
    public function destroy(ContentFile $file);

    /**
     * Сохраняет попытку загрузки файлов
     * @param $model тип модели. Нужно передать название как Model::class, либо же передать инстанс модели
     * @param ContentFile $file
     * @param string $field
     * @return UploadTry
     */
    public function registerUploadTry($model, ContentFile $file, $field = 'file_id');
}
