<?php

namespace App\Services\Content\UploadService;

use Illuminate\Http\UploadedFile;
use App\Models\Content\File as ContentFile;

class ImageUploadService extends UploadService implements ImageUploadServiceInterface
{
    protected $validExtension = [
        'jpg',
        'jpeg',
        'png',
        'gif',
        'bmp',
        'tiff',
    ];

    protected $resize = false;

    public function storeImage(
        UploadedFile $file,
        $directoryPrefix = null,
        $resize = false
    ): ContentFile {
        $this->resize = $resize;
        return parent::storeFile($file, $directoryPrefix);
    }

    public function put($destination, $file)
    {
        if ($this->resize) {
            $image = \Image::make($file)->fit(200);
            return \Storage::disk($this->diskName)->put(
                $destination,
                (string) $image->encode()
            );
        } else {
            return parent::put();
        }
    }
}
