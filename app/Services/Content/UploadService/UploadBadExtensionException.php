<?php


namespace App\Services\Content\UploadService;

use Symfony\Component\HttpFoundation\File\Exception\UploadException;

class UploadBadExtensionException extends UploadException
{
}
