<?php

namespace App\Services\Content;

use App\Services\AbstractService;

class TranslationService extends AbstractService
{
    public function getAvailableLocales()
    {
        $locales = explode("|", config('app.locales'));
        array_unshift($locales, config('app.fallback_locale'));
        return $locales;
    }

    public function getTranslation($locale)
    {
        $localesDirectory = resource_path('lang') . DIRECTORY_SEPARATOR . $locale;
        if (!is_dir($localesDirectory)) {
            return null;
        }

        $messages = [];
        foreach (glob($localesDirectory . DIRECTORY_SEPARATOR . '*.php') as $filename) {
            $locationKey = pathinfo($filename, PATHINFO_FILENAME);
            $messages[strtolower($locationKey)] = require $filename;
        }
        return $messages;
    }

    public function getTranslations()
    {
        $availableLocales = $this->getAvailableLocales();
        $return = [];
        foreach ($availableLocales as $locale) {
            $messages = $this->getTranslation($locale);
            if (!$messages) {
                continue;
            }
            $return[$locale] = $messages;
        }
        return $return;
    }

    public function storeTranslation($locale, $messages)
    {
        $locale = strtolower($locale);
        if (!in_array($locale, $this->getAvailableLocales())) {
            throw new \InvalidArgumentException("Locale [$locale] is not allowed", 403);
        }

        foreach ($messages as $filename => $messages) {
            $targetFile = resource_path('lang')
                . DIRECTORY_SEPARATOR . $locale
                . DIRECTORY_SEPARATOR . $filename . '.php';

            if (!file_exists($targetFile)) {
                if (!is_writable(dirname($targetFile))) {
                    continue;
                }
                touch($targetFile);
            }
            if (!is_writable($targetFile)) {
                throw new \Exception("Locale [$locale] is not writeable", 400);
            }
            file_put_contents($targetFile, '<?php' . PHP_EOL . 'return ' . var_export($messages, 1) . ';');
        }
    }
}
