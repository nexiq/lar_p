<?php

return [
    'common' => [
        'yesno' => [
            'yes' => 1,
            'no' => 0,
        ],
    ],
    'work' => [
        'type' => [
            'regular'   => 0b1,         // 1
            'project'   => 0b10,        // 2
            'single'    => 0b100,       // 4
            'temp'      => 0b1000,      // 8
            'hourly'    => 0b10000,     // 16
            'remote'    => 0b100000,    // 32
        ],
        'schedule' => [
            '5_2'       => 0b1,
            '2_2'       => 0b10,
            'weekend'   => 0b100,
        ],
        'team' => [
            'positive'  => 0b1,
            'negative'  => 0b10,
            'neutral'   => 0b100,
            'project'   => 0b1000,
            'corporate' => 0b10000,
        ],
        'education_type' => [
            'higher'        => 0b1,
            'middle'        => 0b10,
            'professional'  => 0b100,
            'student'       => 0b1000,
        ],
        'education_form' => [
            'fulltime'      => 0b1,
            'extramural'    => 0b10,
            'distance'      => 0b100,
        ],
        'experience' => [
            'none'      => 0b1,
            'less1'     => 0b10,
            'from1to3'  => 0b100,
            'more3'     => 0b1000,
        ],
        'body_type' => [
            'thin'      => 0b1,
            'normal'    => 0b10,
            'thick'     => 0b100,
        ],
        'clothing_size' => [
            'xs'    => 0b1,
            's'     => 0b10,
            'm'     => 0b100,
            'l'     => 0b1000,
            'xl'    => 0b10000,
            'xxl'   => 0b100000,
            'xxxl'  => 0b1000000,
        ],
        'drivers_license' => [
            'A' => 0b1,
            'B' => 0b10,
            'C' => 0b100,
            'D' => 0b1000,
            'E' => 0b10000,
        ],
    ],
    'profile' => [
        'candidate' => [
            'gender' => [
                'male' => 0,
                'female' => 1,
            ],
            'status' => [
                'draft' => 0,
                'unverified' => 1,
                'approved' => 2,
                'declined' => 3,
            ],
        ],
    ],
    'moderation' => [
        'max_moderation_time' => 600,
    ],
];
