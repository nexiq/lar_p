<?php
return [
    /**
     * Поддерживается функции в качестве названий роута
     */
    'homeRedirect' => [
        'admin' => 'backend',
        'employer' => 'home',
        'candidate' => 'home',
    ],

    'registrationRedirect' => [
        'admin' => 'backend',
        'employeer' => 'home',
        'candidate' => function ($user) {
            return route('candidate.edit', [
                'profile' => $user->candidateProfile
            ]);
        },
    ],

    // список ролей которые допускаются к регистрации с фронтенда
    'frontend' => [
        'employer',
        'candidate',
        // 'admin',
    ]
];
