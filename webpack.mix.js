// eslint-disable-next-line import/no-extraneous-dependencies
const mix = require('laravel-mix');
// eslint-disable-next-line import/no-extraneous-dependencies
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

mix.webpackConfig({
  plugins: [
    // new BundleAnalyzerPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        // include: [resolve('resources/js')],
        options: {
          // eslint-disable-next-line global-require,import/no-extraneous-dependencies
          formatter: require('eslint-friendly-formatter'),
          emitWarning: false,
        },
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
    ],
  },
});
mix.extract([
  'vue',
  'axios',
  'lodash',
  'vuex',
  'vue-router',
]);
mix.version();

mix.js('resources/js/main.js', 'public/js/main.js');
mix.js('resources/js/app.js', 'public/js/app.js');
mix.sass('resources/sass/app.scss', 'public/css/app.css');


mix
  .js('resources/js/backend.js', 'public/js/backend.js');

mix.sass('resources/sass/backend.scss', 'public/css/backend.css');
