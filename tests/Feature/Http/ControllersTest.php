<?php

namespace Tests\Feature\Http;

use App\Models\Core\User;
use App\Models\Content\Page;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ControllersTest extends TestCase
{
    public function testMainPage()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testSomePage()
    {
        $page = factory(Page::class)->create();
        $response = $this->get('/'.$page->slug);
        $response->assertStatus(200);
    }

    public function testWrongPage()
    {
        $response = $this->get('/notexisting');
        $response->assertStatus(404);
    }

    public function testBackendGuest()
    {
        $response = $this->get('/backend');
        $response->assertStatus(302);
    }

    public function testBackendAdmin()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->get('/backend');
        $response->assertStatus(200);
    }
}
