<?php

namespace Tests\Feature\Http;

use App\Models\Core\User;
use App\Models\Core\Role;
use App\Models\Core\Permission;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiTestCase extends TestCase
{
    protected $model;
    protected $group;
    protected $user;
    protected $role;
    protected $permission;
    protected $data;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->role = factory(Role::class)->create();

        $this->user->attachRole($this->role);
    }

    public function testForbidden()
    {
        $response = $this->json('GET', "/api/$this->model");
        $response->assertStatus(401);
    }

    protected function prepare($attach = true)
    {
        if ($attach) {
            $this->role->perms()->sync([$this->permission->id]);
        } else {
            $this->role->perms()->sync([]);
        }
        $this->role->save();

        return $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user->api_token,
            'X-Requested-With' => 'XMLHttpRequest'
        ]);
    }

    public function testIndex()
    {
        $this->permission = Permission::where([
            'name' => "$this->group.$this->model.read"
        ])->firstOrFail();

        $response = $this
            ->prepare(false)
            ->json('GET', "/api/$this->model");
        $response->assertStatus(403);

        $response = $this
            ->prepare(true)
            ->json('GET', "/api/$this->model");
        $response->assertStatus(200);
    }

    public function testCreate()
    {
        $this->permission = Permission::where([
            'name' => "$this->group.$this->model.create"
        ])->firstOrFail();

        $response = $this
            ->prepare(false)
            ->json('POST', "/api/$this->model");
        $response->assertStatus(403);

        $response = $this
            ->prepare(true)
            ->json('POST', "/api/$this->model");
        $response->assertStatus(422);

        $response = $this
            ->prepare(true)
            ->json('POST', "/api/$this->model", $this->data);
        $response->assertStatus(201);
        return $response;
    }

    public function testRead()
    {
        $this->permission = Permission::where([
            'name' => "$this->group.$this->model.read"
        ])->firstOrFail();

        $response = $this
            ->prepare(false)
            ->json('GET', "/api/$this->model/1");
        $response->assertStatus(403);

        $response = $this
            ->prepare(true)
            ->json('GET', "/api/$this->model/1");
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $this->permission = Permission::where([
            'name' => "$this->group.$this->model.update"
        ])->firstOrFail();

        $response = $this
            ->prepare(false)
            ->json('PUT', "/api/$this->model/1");
        $response->assertStatus(403);

        $response = $this
            ->prepare(true)
            ->json('PUT', "/api/$this->model/1");
        $response->assertStatus(422);
    }

    public function testDelete()
    {
        $this->permission = Permission::where([
            'name' => "$this->group.$this->model.delete"
        ])->firstOrFail();

        $response = $this
            ->prepare(false)
            ->json('DELETE', "/api/$this->model/" . ($this->modelId ?? 1));
        $response->assertStatus(403);
    }
}
