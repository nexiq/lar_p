<?php

namespace Tests\Feature\Http\Api;

use App\Models\Work\Language;
use Tests\Feature\Http\ApiTestCase;

class LanguageTest extends ApiTestCase
{
    public function __construct()
    {
        parent::__construct();

        $this->group = 'work';
        $this->model = 'languages';
    }

    public function testCreate()
    {
        $page = factory(Language::class)->make();
        $this->data = [
            'translations' => []
        ];
        $locales = array_merge(['ru'], explode('|', config('app.locales')));

        foreach ($locales as $locale) {
            $this->data['translations'][$locale] = [
                'title' => $page->title,
            ];
        }

        parent::testCreate();
    }
}
