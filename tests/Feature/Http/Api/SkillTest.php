<?php

namespace Tests\Feature\Http\Api;

use Tests\Feature\Http\ApiTestCase;
use App\Models\Work\Skill;

class SkillTest extends ApiTestCase
{
    public function __construct()
    {
        parent::__construct();

        $this->group = 'work';
        $this->model = 'skills';
    }

    public function testCreate()
    {
        $page = factory(Skill::class)->make();
        $this->data = [
            'translations' => []
        ];
        $locales = array_merge(['ru'], explode('|', config('app.locales')));

        foreach ($locales as $locale) {
            $this->data['translations'][$locale] = [
                'title' => $page->title,
            ];
        }

        parent::testCreate();
    }
}
