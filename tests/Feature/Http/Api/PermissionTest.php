<?php

namespace Tests\Feature\Http\Api;

use Tests\Feature\Http\ApiTestCase;

class PermissionTest extends ApiTestCase
{
    public function __construct()
    {
        parent::__construct();

        $this->group = 'core';
        $this->model = 'permissions';

        $this->data = [
            'name' => 'Name'
        ];
    }

    public function testCreate()
    {
        $response = $this
            ->prepare(false)
            ->json('POST', "/api/$this->model");
        $response->assertStatus(403);
    }

    public function testUpdate()
    {
        $response = $this
            ->prepare(false)
            ->json('PUT', "/api/$this->model/1");
        $response->assertStatus(403);
    }

    public function testDelete()
    {
        $response = $this
            ->prepare(false)
            ->json('DELETE', "/api/$this->model/1");
        $response->assertStatus(403);
    }
}
