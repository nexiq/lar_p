<?php

namespace Tests\Feature\Http\Api;

use App\Models\Work\Country;

class CountryTest
{
    public function __construct()
    {
        parent::__construct();

        $this->group = 'work';
        $this->model = 'countries';
    }

    public function testCreate()
    {
        $page = factory(Country::class)->make();
        $this->data = [
            'translations' => []
        ];
        $locales = array_merge(['ru'], explode('|', config('app.locales')));

        foreach ($locales as $locale) {
            $this->data['translations'][$locale] = [
                'name' => $page->name,
            ];
        }

        parent::testCreate();
    }
}
