<?php

namespace Tests\Feature\Http\Api;

use Tests\Feature\Http\ApiTestCase;
use App\Models\Content\Page;

class PageTest extends ApiTestCase
{
    public function __construct()
    {
        parent::__construct();

        $this->group = 'content';
        $this->model = 'pages';
    }

    public function testCreate()
    {
        $page = factory(Page::class)->make();
        $this->data = [
            'slug' => $page->slug,
            'translations' => []
        ];
        $locales = array_merge(['ru'], explode('|', config('app.locales')));

        foreach ($locales as $locale) {
            $this->data['translations'][$locale] = [
                'title' => $page->title,
                'description' => $page->description,
                'content' => $page->content,
            ];
        }

        parent::testCreate();
    }
}
