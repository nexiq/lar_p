<?php

namespace Tests\Feature\Http\Api;

use Tests\Feature\Http\ApiTestCase;
use App\Models\Work\City;

class CityTest extends ApiTestCase
{
    public function __construct()
    {
        parent::__construct();

        $this->group = 'work';
        $this->model = 'cities';
    }

    public function testCreate()
    {
        $page = factory(City::class)->make();
        $this->data = [
            'translations' => []
        ];
        $locales = array_merge(['ru'], explode('|', config('app.locales')));

        foreach ($locales as $locale) {
            $this->data['translations'][$locale] = [
                'name' => $page->name,
            ];
        }

        parent::testCreate();
    }
}
