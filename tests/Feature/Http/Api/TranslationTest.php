<?php

namespace Tests\Feature\Http\Api;

use App\Models\Core\Permission;
use App\Models\Core\Role;
use App\Models\Core\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class TranslationTest extends TestCase
{
    protected $model;
    protected $group;
    protected $user;
    protected $role;
    protected $permission;
    protected $data;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->role = factory(Role::class)->create();

        $this->user->attachRole($this->role);
    }

    public function __construct()
    {
        parent::__construct();

        $this->group = 'content';
        $this->model = 'translations';
    }

    protected function prepare($attach = true)
    {
        $readPermission = Permission::where([
            'name' => "$this->group.$this->model.read"
        ])->firstOrFail();
        $writePermission = Permission::where([
            'name' => "$this->group.$this->model.update"
        ])->firstOrFail();

        if ($attach) {
            $this->role->perms()->sync([$readPermission->id, $writePermission->id]);
        } else {
            $this->role->perms()->sync([]);
        }
        $this->role->save();

        return $this->withHeaders([
            'Authorization' => 'Bearer ' . $this->user->api_token
        ]);
    }

    public function testGetAllTranslationsForbidden()
    {
        $response = $this
            ->prepare(false)
            ->json('GET', "/api/$this->model");
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testGetAllTranslationsAllow()
    {
        $response = $this
            ->prepare(true)
            ->json('GET', "/api/$this->model");
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testRussianTranslation()
    {
        $response = $this
            ->prepare(true)
            ->json('GET', "/api/$this->model/ru");
        $response->assertStatus(Response::HTTP_OK);
        $someData = require resource_path('lang')
            . DIRECTORY_SEPARATOR . 'ru'
            . DIRECTORY_SEPARATOR . 'auth.php';
        $response->assertJson([
            'data' => [
                'locale' => 'ru',
                'messages' => [
                    'auth' => $someData
                ],
            ]
        ]);
    }

    public function testUpdateLocaleWithEmptyTranslation()
    {
        $response = $this
            ->prepare(true)
            ->json('PUT', "/api/$this->model/uk", [
                'translation' => [],
            ]);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdateWrongLocale()
    {
        $response = $this
            ->prepare(true)
            ->json('PUT', "/api/$this->model/zz", [
                'translation' => [],
            ]);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
