<?php

namespace Tests\Feature\Http\Api;

use App\Models\Work\Position;
use Tests\Feature\Http\ApiTestCase;

class PositionTest extends ApiTestCase
{
    public function __construct()
    {
        parent::__construct();

        $this->group = 'work';
        $this->model = 'positions';
    }

    public function testCreate()
    {
        $page = factory(Position::class)->make();
        $this->data = [
            'translations' => []
        ];
        $locales = array_merge(['ru'], explode('|', config('app.locales')));

        foreach ($locales as $locale) {
            $this->data['translations'][$locale] = [
                'title' => $page->title,
            ];
        }

        parent::testCreate();
    }
}
