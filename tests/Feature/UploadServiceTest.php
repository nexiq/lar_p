<?php

namespace Tests\Feature\Http;

use App\Models\Content\Page;
use App\Models\Content\UploadTry;
use App\Models\Core\Role;
use App\Models\Core\User;
use App\Services\Content\UploadService\ImageUploadService;
use App\Services\Content\UploadService\UploadBadExtensionException;
use App\Services\Content\UploadService\UploadService;
use App\Services\Content\UploadService\UploadServiceInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class UploadServiceTest extends TestCase
{
    /**
     * @var User
     */
    protected $user;
    protected $role;


    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->role = Role::where('id', 1)->first();
        $this->user->attachRole($this->role);
        $this->actingAs($this->user);
    }

    public function testUploadBadExtensionFile()
    {
        $testFile = UploadedFile::fake()->create('1.php', 1024 * 1024);
        $uploadService = new UploadService();

        $this->expectException(UploadBadExtensionException::class);
        $uploadService->storeFile($testFile);
    }

    public function testUploadValidFile()
    {
        $fileToUpload = base_path() . DIRECTORY_SEPARATOR . 'readme.md';
        $testFile = new UploadedFile($fileToUpload, 'readme.md');

        $uploadService = new UploadService();


        $contentFile = $uploadService->storeFile($testFile);
        $this->assertInstanceOf(\App\Models\Content\File::class, $contentFile);
        $this->assertEquals(filesize($fileToUpload), $contentFile->size);
        $this->assertEquals('readme.md', $contentFile->name);
        $this->assertEquals('md', $contentFile->extension);
        $this->assertEquals($this->user->id, $contentFile->user_id);
        $this->assertTrue($contentFile->url == filter_var($contentFile->url, FILTER_VALIDATE_URL));

        if (windows_os()) {
            $contentFile->url = str_replace('\\', '/', $contentFile->url);
        }
        $localPath = parse_url($contentFile->url, PHP_URL_PATH);
        $this->assertFileExists(public_path() . $localPath);
        $this->assertEquals(file_get_contents(public_path() . $localPath), file_get_contents($fileToUpload));
    }

    public function testUploadValidFileWithPrefixAndDelete()
    {
        $fileToUpload = base_path() . DIRECTORY_SEPARATOR . 'readme.md';
        $fileToUpload = base_path() . DIRECTORY_SEPARATOR . 'readme.md';
        $testFile = new UploadedFile($fileToUpload, 'readme.md');
        $uploadService = new UploadService();


        $contentFile = $uploadService->storeFile($testFile, 'avatars');
        $this->assertTrue($contentFile->wasRecentlyCreated);
        $this->assertInstanceOf(\App\Models\Content\File::class, $contentFile);
        $this->assertEquals(filesize($fileToUpload), $contentFile->size);
        $this->assertEquals('readme.md', $contentFile->name);
        $this->assertEquals(config('filesystems.default'), $contentFile->disk);
        $this->assertEquals('md', $contentFile->extension);
        $this->assertEquals($this->user->id, $contentFile->user_id);
        $this->assertTrue($contentFile->url == filter_var($contentFile->url, FILTER_VALIDATE_URL));

        if (windows_os()) {
            $contentFile->url = str_replace('\\', '/', $contentFile->url);
        }
        $localPath = parse_url($contentFile->url, PHP_URL_PATH);
        $this->assertFileExists(public_path() . $localPath);
        $this->assertEquals(file_get_contents(public_path() . $localPath), file_get_contents($fileToUpload));

        $this->assertTrue(\App\Models\Content\File::where('id', $contentFile->id)->exists());
        $uploadService->destroy($contentFile);
        clearstatcache();


        $this->assertFileNotExists(public_path() . $localPath);
        $this->assertFalse(\App\Models\Content\File::where('id', $contentFile->id)->exists());
    }

    public function testDependencyInjection()
    {
        $depService = $this->app->make(UploadServiceInterface::class);
        $this->assertInstanceOf(UploadService::class, $depService);
    }

    public function testImageServiceDependencyInjection()
    {
        $depService = $this->app->make(ImageUploadService::class);
        $this->assertInstanceOf(ImageUploadService::class, $depService);
    }

    public function testImageUploadServiceFailed()
    {
        $fileToUpload = base_path() . DIRECTORY_SEPARATOR . 'readme.md';
        $testFile = new UploadedFile($fileToUpload, 'readme.md');
        $uploadService = new ImageUploadService();

        $this->expectException(UploadBadExtensionException::class);
        $uploadService->storeFile($testFile, 'avatars');
    }

    public function testDeadFilesRemove()
    {
        UploadTry::query()->delete();
        \App\Models\Content\File::query()->delete();

        $this->assertEquals(0, UploadTry::query()->count());
        $this->assertEquals(0, \App\Models\Content\File::query()->count());

        $fileToUpload = base_path() . DIRECTORY_SEPARATOR . 'readme.md';
        $testFile = new UploadedFile($fileToUpload, 'readme.md');
        $uploadService = new UploadService();
        $contentFile = $uploadService->storeFile($testFile);

        $this->assertInstanceOf(\App\Models\Content\File::class, $contentFile);


        $result = $uploadService->registerUploadTry(Page::class, $contentFile, 'some_field');
        $this->assertInstanceOf(UploadTry::class, $result);
        $this->assertEquals(1, UploadTry::query()->count());
        $this->assertEquals(1, \App\Models\Content\File::query()->count());

        UploadService::cleanUploadTries($this->user->email);
        $this->assertEquals(0, UploadTry::query()->count());
        $this->assertEquals(0, \App\Models\Content\File::query()->count());
    }
}
