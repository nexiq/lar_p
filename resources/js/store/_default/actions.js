export const UPDATE_CURRENT = (context, payload) => {
  context.commit('updateCurrent', payload);
};

export const SET_CURRENT = (context, payload) => {
  context.commit('setCurrent', payload);
};

export const RESET_CURRENT = (context) => {
  context.commit('resetCurrent');

  if (context.state.multilang) {
    const translations = {};
    context.rootState.content.translations.locales.forEach((locale) => {
      translations[locale] = {};
    });
    context.commit('updateCurrent', {
      key: 'translations',
      value: translations,
    });
  }
};

export const LOAD_LIST = (context) => {
  context.dispatch('app/CALL_API', {
    url: `/api/${context.state.name}`,
    method: 'get',
    onSuccess: `${context.state.group}/${context.state.name}/loadList`,
  }, { root: true });
};
