export const getList = (state) => state.list;

export const getCount = (state) => Object.keys(state.list).length;

export const getCurrent = (state) => state.current;
