import Vue from 'vue';

export const loadList = (state, payload) => {
  Vue.set(state, 'list', payload.data);
};

export const loadCurrent = (state, payload) => {
  Vue.set(state, 'current', payload.data);
};

export const create = (state, payload) => {
  const item = payload.data;
  Vue.set(state.list, item.id, item);
};

export const updateCurrent = (state, payload) => {
  if (state.multilang && payload.locale) {
    if (!state.current.translations) {
      Vue.set(state.current, 'translations', {});
    }
    if (!state.current.translations[payload.locale]) {
      Vue.set(state.current.translations, payload.locale, {});
    }
    Vue.set(state.current.translations[payload.locale], payload.key, payload.value);
  } else {
    const path = payload.key.split('/');
    if (path.length > 1) {
      let { current } = state;
      for (let i = 0; i < path.length - 1; i += 1) {
        current = current[path[i]];
      }
      Vue.set(current, path[path.length - 1], payload.value);
    } else {
      Vue.set(state.current, payload.key, payload.value);
    }
  }
};

export const setCurrent = (state, payload) => {
  state.current = payload;
};

export const resetCurrent = (state) => {
  state.current = {};
};

export const remove = (state, id) => {
  Vue.delete(state.list, id);
};
