import Vue from 'vue';
import Vuex from 'vuex';
import app from './app';
import core from './core';
import content from './content';
import work from './work';
import profile from './profile';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    app,
    core,
    content,
    work,
    profile,
  },
});
