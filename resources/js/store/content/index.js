import translations from './translations';
import pages from './pages';

export default {
  namespaced: true,
  modules: {
    translations,
    pages,
  },
};
