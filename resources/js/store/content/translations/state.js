export default {
  list: window.translations.messages,
  current: {},
  locales: window.locales,
  source: '',
  target: '',
};
