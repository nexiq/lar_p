export const loaded = (state) => (locale) => !!state.list[locale];

export const sourceMessages = (state) => state.list[state.source];

export const targetMessages = (state) => state.list[state.target];

export const ready = (state) => state.list[state.source] && state.list[state.target];

export const getTargetValue = (state) => (path) => {
  let res = state.list[state.target];

  while (path.length) {
    const key = path.shift();
    res = res[key] || '';
  }

  return res;
};
