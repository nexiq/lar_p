export const SET_LOCALE = (context, payload) => {
  const { type, locale } = payload;
  context.commit(type == 'source' ? 'setSource' : 'setTarget', locale);
  const loaded = context.getters.loaded(locale);

  if (!loaded) {
    context.dispatch('app/CALL_API', {
      url: `/api/translations/${locale}`,
      onSuccess: 'content/translations/load',
    }, { root: true });
  }
};
