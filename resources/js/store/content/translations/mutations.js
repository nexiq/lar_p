import Vue from 'vue';

export const load = (state, payload) => {
  const { locale, messages } = payload.data;
  Vue.set(state.list, locale, messages);
};

export const setSource = (state, payload) => {
  state.source = payload;
};

export const setTarget = (state, payload) => {
  state.target = payload;
};

export const update = (state, payload) => {
  const { path, value } = payload;

  let res = state.list[state.target];
  let key;

  while (path.length > 1) {
    key = path.shift();
    if (res[key]) {
      res = res[key];
    } else {
      Vue.set(res, key, {});
      res = res[key];
    }
  }

  Vue.set(res, path[0], value);
};

export const save = (state, payload) => {
  // eslint-disable-next-line
  console.log(state, payload);
};
