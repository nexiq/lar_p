import Vue from 'vue';

export const setLoading = (state, payload) => {
  state.loading += payload ? 1 : -1;
};

export const setError = (state, payload) => {
  state.error = payload.errors || {};
  Vue.toasted.error(payload.message);
};

export const resetError = (state) => {
  state.error = {};
};

export const loadUser = (state, payload) => {
  const user = payload.data;
  state.user = user;

  Object.values(user.roles).forEach((role) => {
    Object.values(role.permissions).forEach((perm) => {
      state.permissions.push(perm.name);
    });
  });
};
