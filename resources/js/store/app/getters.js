export const dataList = (state, getters, rootState) => (path) => {
  const chunks = path.split('.');

  let { constants } = rootState.app;
  let translations = rootState.content.translations.list[window.translations.locale].constants;

  while (chunks.length) {
    const chunk = chunks.shift();

    constants = constants[chunk] || {};
    translations = translations[chunk] || {};
  }

  const res = {};

  Object.keys(constants).forEach((key) => {
    res[constants[key]] = translations[key] || `${path}.${key}`;
  });

  return res;
};
