export default {
  loading: 0,
  error: {},
  user: {},
  permissions: [],
  constants: window.constants,
  previewLink: window.previewLink,
};
