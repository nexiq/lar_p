import Vue from 'vue';
import axios from 'axios';

export const setUser = (context, payload) => {
  context.commit('loadUser', { data: payload });
};

export const CALL_API = (context, payload) => {
  context.commit('setLoading', true);
  context.commit('resetError');

  const query = {
    method: payload.method || 'get',
    url: payload.url,
  };
  query.data = payload.data;

  return axios(query)
    .then((response) => {
      if (payload.notify) {
        Vue.toasted.success(payload.notify);
      }
      if (payload.onSuccess) {
        context.commit(payload.onSuccess, response.data, { root: true });
      }
      return response;
    })
    .catch((err) => {
      if (err.response && err.response.data) {
        context.commit('setError', err.response.data);
      } else {
        context.commit('setError', { message: err.message });
      }
    })
    .finally(() => {
      context.commit('setLoading', false);
    });
};

export const UPLOAD = (context, payload) => {
  context.commit('setLoading', true);
  context.commit('resetError');

  const formData = new FormData();
  formData.append('file', payload.file);

  const query = {
    method: 'post',
    url: payload.url,
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    data: formData,
  };

  return axios(query)
    .then((response) => {
      if (payload.notify) {
        Vue.toasted.success(payload.notify);
      }
      if (payload.onSuccess) {
        context.commit(payload.onSuccess, response.data, { root: true });
      }
      return response;
    })
    .catch((err) => {
      if (err.response && err.response.data) {
        context.commit('setError', err.response.data);
      } else {
        context.commit('setError', { message: err.message });
      }
    })
    .finally(() => {
      context.commit('setLoading', false);
    });
};
