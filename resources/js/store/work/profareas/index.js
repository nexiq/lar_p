import state from './state';
import * as getters from '../../_default/getters';
import * as mutations from '../../_default/mutations';
import * as actions from '../../_default/actions';

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
