import qualities from './qualities';
import skills from './skills';
import profareas from './profareas';
import cities from './cities';
import regions from './regions';
import countries from './countries';
import specialities from './specialities';
import positions from './positions';
import languages from './languages';

export default {
  namespaced: true,
  modules: {
    qualities,
    skills,
    profareas,
    cities,
    regions,
    countries,
    specialities,
    positions,
    languages,
  },
};
