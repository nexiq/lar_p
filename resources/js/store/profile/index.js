import candidate from './candidate';

export default {
  namespaced: true,
  modules: {
    candidate,
  },
};
