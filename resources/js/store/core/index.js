import users from './users';
import roles from './roles';
import permissions from './permissions';

export default {
  namespaced: true,
  modules: {
    users,
    roles,
    permissions,
  },
};
