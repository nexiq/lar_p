import Vue from 'vue';

// eslint-disable-next-line import/prefer-default-export
export const loadList = (state, payload) => {
  const permissions = payload.data;

  Object.keys(permissions).forEach((key) => {
    const permission = permissions[key];
    const [group, model, action] = permission.name.split('.');

    if (!state.list[group]) Vue.set(state.list, group, {});

    if (!state.list[group][model]) Vue.set(state.list[group], model, {});

    Vue.set(state.list[group][model], action, permission);
  });
};
