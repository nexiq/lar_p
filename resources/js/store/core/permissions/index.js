import state from './state';
import * as getters from '../../_default/getters';
import * as mutations from './mutations';
import { LOAD_LIST } from '../../_default/actions';

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions: {
    LOAD_LIST,
  },
};
