import Vue from 'vue';

import store from './store/frontend';
import router from './router/frontend';
import i18n from './init/i18n';


require('./bootstrap');
require('./init/Toasted');
require('./mixins/can');
require('./mixins/deep');

window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

new Vue({
  i18n,
  store,
  router,
  created() {

  },
}).$mount('#app');
