import Vue from 'vue';

Vue.mixin({
  methods: {
    can(permission) {
      return this.$store.state.app.permissions.includes(permission);
    },
  },
});
