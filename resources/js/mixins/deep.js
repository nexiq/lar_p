import Vue from 'vue';

Vue.mixin({
  methods: {
    $deepGet(object, path) {
      return path.split('/').reduce(
        (xs, x) => ((xs && xs[x]) ? xs[x] : null), object,
      );
    },
  },
});
