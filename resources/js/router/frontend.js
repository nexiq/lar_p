import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './frontend/routes';

Vue.use(VueRouter);

export default new VueRouter({
  routes,
  linkExactActiveClass: 'active',
});
