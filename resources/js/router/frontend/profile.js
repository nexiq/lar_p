import CandidateForm from '../../components/profile/candidate/Form.vue';

export default [
  {
    path: '',
    name: 'profile.candidate.edit',
    meta: {
      permission: 'profile.candidates.update',
    },
    component: CandidateForm,
  },
];
