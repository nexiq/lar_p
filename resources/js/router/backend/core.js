import ExampleComponent from '../../components/ExampleComponent.vue';
import UsersList from '../../components/core/users/List.vue';
import UsersForm from '../../components/core/users/Form.vue';
import RolesList from '../../components/core/roles/List.vue';
import RolesForm from '../../components/core/roles/Form.vue';

export default [
  {
    path: '/',
    name: 'home',
    component: ExampleComponent,
  },
  {
    path: '/users',
    component: UsersList,
    name: 'users',
    meta: { permission: 'core.users.read' },
  },
  {
    path: '/users/create',
    component: UsersForm,
    name: 'users.create',
    meta: { permission: 'core.users.create' },
  },
  {
    path: '/users/edit/:id',
    component: UsersForm,
    name: 'users.edit',
    meta: { permission: 'core.users.update' },
  },
  {
    path: '/roles',
    component: RolesList,
    name: 'roles',
    meta: { permission: 'core.roles.read' },
  },
  {
    path: '/roles/edit/:id',
    component: RolesForm,
    name: 'roles.edit',
    meta: { permission: 'core.roles.update' },
  },
];
