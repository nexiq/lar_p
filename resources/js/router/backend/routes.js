import core from './core';
import content from './content';
import work from './work';

export default [
  ...core,
  ...content,
  ...work,
];
