import QualityList from '../../components/work/qualities/List.vue';
import QualityForm from '../../components/work/qualities/Form.vue';

import SkillList from '../../components/work/skills/List.vue';
import SkillForm from '../../components/work/skills/Form.vue';

import ProfareaList from '../../components/work/profareas/List.vue';
import ProfareaForm from '../../components/work/profareas/Form.vue';

import CityList from '../../components/work/cities/List.vue';
import CityForm from '../../components/work/cities/Form.vue';

import RegionList from '../../components/work/regions/List.vue';
import RegionForm from '../../components/work/regions/Form.vue';

import CountryList from '../../components/work/countries/List.vue';
import CountryForm from '../../components/work/countries/Form.vue';

import SpecialityList from '../../components/work/specialities/List.vue';
import SpecialityForm from '../../components/work/specialities/Form.vue';

import PositionList from '../../components/work/positions/List.vue';
import PositionForm from '../../components/work/positions/Form.vue';

import LanguageList from '../../components/work/languages/List.vue';
import LanguageForm from '../../components/work/languages/Form.vue';

export default [
  {
    path: '/qualities',
    component: QualityList,
    name: 'qualities',
    meta: { permission: 'work.qualities.read' },
  },
  {
    path: '/qualities/create',
    component: QualityForm,
    name: 'qualities.create',
    meta: { permission: 'work.qualities.create' },
  },
  {
    path: '/qualities/edit/:id',
    component: QualityForm,
    name: 'qualities.edit',
    meta: { permission: 'work.qualities.update' },
  },
  {
    path: '/skills',
    component: SkillList,
    name: 'skills',
    meta: { permission: 'work.skills.read' },
  },
  {
    path: '/skills/create',
    component: SkillForm,
    name: 'skills.create',
    meta: { permission: 'work.skills.create' },
  },
  {
    path: '/skills/edit/:id',
    component: SkillForm,
    name: 'skills.edit',
    meta: { permission: 'work.skills.update' },
  },
  {
    path: '/profareas',
    component: ProfareaList,
    name: 'profareas',
    meta: { permission: 'work.profareas.read' },
  },
  {
    path: '/profareas/create',
    component: ProfareaForm,
    name: 'profareas.create',
    meta: { permission: 'work.profareas.create' },
  },
  {
    path: '/profareas/edit/:id',
    component: ProfareaForm,
    name: 'profareas.edit',
    meta: { permission: 'work.profareas.update' },
  },
  {
    path: '/cities',
    component: CityList,
    name: 'cities',
    meta: { permission: 'work.cities.read' },
  },
  {
    path: '/cities/create',
    component: CityForm,
    name: 'cities.create',
    meta: { permission: 'work.cities.create' },
  },
  {
    path: '/cities/edit/:id',
    component: CityForm,
    name: 'cities.edit',
    meta: { permission: 'work.cities.update' },
  },
  {
    path: '/countries',
    component: CountryList,
    name: 'countries',
    meta: { permission: 'work.countries.read' },
  },
  {
    path: '/countries/create',
    component: CountryForm,
    name: 'countries.create',
    meta: { permission: 'work.countries.create' },
  },
  {
    path: '/countries/edit/:id',
    component: CountryForm,
    name: 'countries.edit',
    meta: { permission: 'work.countries.update' },
  },
  {
    path: '/regions',
    component: RegionList,
    name: 'regions',
    meta: { permission: 'work.regions.read' },
  },
  {
    path: '/regions/create',
    component: RegionForm,
    name: 'regions.create',
    meta: { permission: 'work.regions.create' },
  },
  {
    path: '/regions/edit/:id',
    component: RegionForm,
    name: 'regions.edit',
    meta: { permission: 'work.regions.update' },
  },
  {
    path: '/specialities',
    component: SpecialityList,
    name: 'specialities',
    meta: { permission: 'work.specialities.read' },
  },
  {
    path: '/specialities/create',
    component: SpecialityForm,
    name: 'specialities.create',
    meta: { permission: 'work.specialities.create' },
  },
  {
    path: '/specialities/edit/:id',
    component: SpecialityForm,
    name: 'specialities.edit',
    meta: { permission: 'work.specialities.update' },
  },
  {
    path: '/positions',
    component: PositionList,
    name: 'positions',
    meta: { permission: 'work.positions.read' },
  },
  {
    path: '/positions/create',
    component: PositionForm,
    name: 'positions.create',
    meta: { permission: 'work.positions.create' },
  },
  {
    path: '/positions/edit/:id',
    component: PositionForm,
    name: 'positions.edit',
    meta: { permission: 'work.positions.update' },
  },
  {
    path: '/languages',
    component: LanguageList,
    name: 'languages',
    meta: { permission: 'work.languages.read' },
  },
  {
    path: '/languages/create',
    component: LanguageForm,
    name: 'languages.create',
    meta: { permission: 'work.languages.create' },
  },
  {
    path: '/languages/edit/:id',
    component: LanguageForm,
    name: 'languages.edit',
    meta: { permission: 'work.languages.update' },
  },
];
