import PagesList from '../../components/content/pages/List.vue';
import PagesForm from '../../components/content/pages/Form.vue';

import TranslationList from '../../components/content/translations/List.vue';
// import TranslationsEdit from '../../components/content/translations/Edit.vue';

export default [
  {
    path: '/translations',
    name: 'translations',
    meta: {
      permission: 'content.translations.read',
    },
    component: TranslationList,
  },
  // {
  //   path: '/translations/:locale',
  //   name: 'translations.edit',
  //   meta: {
  //     permission: 'content.translations.update',
  //   },
  //   component: TranslationsEdit,
  // },
  {
    path: '/pages',
    component: PagesList,
    name: 'pages',
    meta: { permission: 'content.pages.read' },
  },
  {
    path: '/pages/create',
    component: PagesForm,
    name: 'pages.create',
    meta: { permission: 'content.pages.create' },
  },
  {
    path: '/pages/edit/:id',
    component: PagesForm,
    name: 'pages.edit',
    meta: { permission: 'content.pages.update' },
  },
];
