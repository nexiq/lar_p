import Vue from 'vue';

import store from './store/backend';
import router from './router/backend';
import i18n from './init/i18n';
import AdminMenu from './components/core/AdminMenu.vue';
import LangSwitcher from './components/core/LangSwitcher.vue';
import Loading from './components/_common/Loading.vue';

require('./bootstrap');
require('./init/Toasted');
require('./mixins/can');

Vue.component('loading', Loading);

new Vue({
  i18n,
  store,
  router,
  created() {
    store.commit('app/loadUser', {
      data: window.user,
    });
  },
  components: {
    AdminMenu,
    LangSwitcher,
  },
}).$mount('#app');

router.beforeEach((to, from, next) => {
  to.matched.forEach((record) => {
    if (record.meta.permission) {
      if (!store.state.app.permissions.includes(record.meta.permission)) {
        next({
          path: '/',
        });
      } else {
        next();
      }
    } else {
      next();
    }
  });
});
