<?php
return array(
    'button' =>
        array(
            'save' => 'Сохранить',
            'edit' => 'Редактировать',
            'preview' => 'Просмотреть',
        ),
    'label' =>
        array(
            'search_query' => 'Поиск...',
            'select' => 'Выбрать',
            'add' => 'Добавить',
            'password' => 'Пароль',
            'password_confirmed' => 'Повторите пароль',
        ),
);
