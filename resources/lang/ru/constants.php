<?php
return [
    'common' => [
        'yesno' => [
            'yes' => 'Да',
            'no' => 'Нет',
        ],
    ],
    'work' => [
        'type' => [
            'regular'   => 'Постоянная',
            'project'   => 'Проектная',
            'single'    => 'Разовая',
            'temp'      => 'Временная',
            'hourly'    => 'Подработка на часы',
            'remote'    => 'Удаленная',
        ],
        'schedule' => [
            '5_2'       => '5/2',
            '2_2'       => '2/2',
            'weekend'   => 'Подработка на выходные',
        ],
        'team' => [
            'positive'  => 'Позитивная',
            'negative'  => 'Негативная',
            'neutral'   => 'Нейтральная',
            'project'   => 'Проектная',
            'corporate' => 'Корпоративы по пятницам',
        ],
        'education_type' => [
            'higher' => 'Высшее',
            'middle' => 'Среднее специальное',
            'professional' => 'Профессиональное',
            'student' => 'Студент',
        ],
        'education_form' => [
            'fulltime' => 'Очная',
            'extramural' => 'Заочная',
            'distance' => 'Дистанционная',
        ],
        'experience' => [
            'none' => 'Нет',
            'less1' => 'Менее 1 года',
            'from1to3' => '1-3 года',
            'more3' => 'Более 3-х лет',
        ],
        'body_type' => [
            'thin' => 'Худощавое',
            'normal' => 'Нормальное',
            'thick' => 'Полное',
        ],
        'clothing_size' => [
            'xs' => 'XS',
            's' => 'S',
            'm' => 'M',
            'l' => 'L',
            'xl' => 'XL',
            'xxl' => 'XXL',
            'xxxl' => 'XXXL',
        ],
        'drivers_license' => [
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'E' => 'E',
        ],
    ],
    'profile' => [
        'candidate' => [
            'gender' => [
                'male' => 'Мужской',
                'female' => 'Женский',
            ],
        ]
    ],
];
