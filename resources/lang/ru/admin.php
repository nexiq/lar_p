<?php
return [
    'home' =>
        [
            'menu' => 'Панель управления',
            'core' => 'Ядро',
            'content' => 'Контент',
            'work' => 'Работа',
            'profile' => 'Профили',
        ],
    'users' =>
        [
            'menu' => 'Пользователи',
            'listTable' =>
                [
                    'name' => 'Имя',
                    'email' => 'Email',
                    'roles' => 'Роли',
                    'phone' => 'Телефон',
                    'phone_ext' => 'Доб. телефон',
                ],
            'notifications' =>
                [
                    'saved' => 'Пользователь сохранен',
                ],
            'actions' => [
                'delete_ask' => 'Вы точно хотите удалить пользователя? Это действие необратимо!',
                'deleted_successful' => 'Пользователь удален',
                'saved_successful' => 'Пользователь сохранен',
            ]
        ],
    'roles' =>
        [
            'menu' => 'Роли',
            'listTable' => [
                'name' => 'Название',
            ],
            'editForm' => [
                'name' => 'Название',
                'desc' => 'Описание',
                'perm-read' => 'Читать',
                'perm-create' => 'Создавать',
                'perm-update' => 'Изменять',
                'perm-delete' => 'Удалять',
            ],
            'actions' => [
                'saved_successful' => 'Роль сохранена',
            ]
        ],
    'translations' =>
        [
            'menu' => 'Переводы',
            'source' => 'Исходный язык',
            'target' => 'Переводимый язык',
            'select' => 'Выбрать...',
            'action' => [
                'saved_successful' => 'Перевод сохранен',
            ]
        ],
    'pages' =>
        [
            'menu' => 'Статические страницы',
            'listTable' => [
                'slug' => 'Алиас',
                'title' => 'Заголовок',
            ],
            'editForm' => [
                'slug' => 'Алиас',
                'title' => 'Заголовок',
                'desc' => 'Meta Description',
                'content' => 'Контент',
            ],
            'actions' => [
                'saved_successful' => 'Страница сохранена',
                'delete_ask' => 'Вы точно хотите удалить страницу? Это действие необратимо!',
                'deleted_successful' => 'Страница удалена',
            ]
        ],
    'qualities' =>
        [
            'menu' => 'Личные качества',
            'listTable' => [
                'title' => 'Название',
            ],
            'editForm' => [
                'title' => 'Название',
            ],
            'actions' => [
                'saved_successful' => 'Качество сохранено',
                'delete_ask' => 'Вы точно хотите удалить элемент? Это действие необратимо!',
                'deleted_successful' => 'Качество удалено',
            ]
        ],
    'skills' =>
        [
            'menu' => 'Навыки',
            'listTable' => [
                'title' => 'Название',
            ],
            'editForm' => [
                'title' => 'Название',
            ],
            'actions' => [
                'saved_successful' => 'Навык сохранен',
                'delete_ask' => 'Вы точно хотите удалить элемент? Это действие необратимо!',
                'deleted_successful' => 'Навык удален',
            ]
        ],
    'profareas' =>
        [
            'menu' => 'Профобласти',
            'listTable' => [
                'title' => 'Название',
            ],
            'editForm' => [
                'title' => 'Название',
            ],
            'actions' => [
                'saved_successful' => 'Профобласть сохранена',
                'delete_ask' => 'Вы точно хотите удалить элемент? Это действие необратимо!',
                'deleted_successful' => 'Профобласть удалена',
            ]
        ],
    'cities' =>
        [
            'menu' => 'Города',
            'listTable' => [
                'name' => 'Название',
            ],
            'editForm' => [
                'name' => 'Название',
                'region' => 'Регион',
            ],
            'actions' => [
                'saved_successful' => 'Город сохранен',
                'delete_ask' => 'Вы точно хотите удалить элемент? Это действие необратимо!',
                'deleted_successful' => 'Город удален',
            ]
        ],
    'regions' =>
        [
            'menu' => 'Регионы',
            'listTable' => [
                'name' => 'Название',
            ],
            'editForm' => [
                'name' => 'Название',
                'country' => 'Страна',
            ],
            'actions' => [
                'saved_successful' => 'Регион сохранен',
                'delete_ask' => 'Вы точно хотите удалить элемент? Это действие необратимо!',
                'deleted_successful' => 'Регион удален',
            ]
        ],
    'countries' =>
        [
            'menu' => 'Страны',
            'listTable' => [
                'name' => 'Название',
            ],
            'editForm' => [
                'name' => 'Название',
            ],
            'actions' => [
                'saved_successful' => 'Страна сохранена',
                'delete_ask' => 'Вы точно хотите удалить элемент? Это действие необратимо!',
                'deleted_successful' => 'Страна удалена',
            ]
        ],
    'specialities' =>
        [
            'menu' => 'Специальности',
            'listTable' => [
                'title' => 'Название',
            ],
            'editForm' => [
                'title' => 'Название',
            ],
            'actions' => [
                'saved_successful' => 'Специальность сохранена',
                'delete_ask' => 'Вы точно хотите удалить элемент? Это действие необратимо!',
                'deleted_successful' => 'Специальность удалена',
            ]
        ],
    'positions' =>
        [
            'menu' => 'Должности',
            'listTable' => [
                'title' => 'Название',
            ],
            'editForm' => [
                'title' => 'Название',
            ],
            'actions' => [
                'saved_successful' => 'Должность сохранена',
                'delete_ask' => 'Вы точно хотите удалить элемент? Это действие необратимо!',
                'deleted_successful' => 'Должность удалена',
            ]
        ],
    'languages' =>
        [
            'menu' => 'Языки',
            'listTable' => [
                'title' => 'Название',
            ],
            'editForm' => [
                'title' => 'Название',
            ],
            'actions' => [
                'saved_successful' => 'Язык сохранен',
                'delete_ask' => 'Вы точно хотите удалить элемент? Это действие необратимо!',
                'deleted_successful' => 'Язык удален',
            ]
        ],
    'files' => ['menu' => 'Файлы'],
    'personal' => ['menu' => 'Личные данные'],
    'permissions' => ['menu' => 'Разрешения'],
    'candidates' => ['menu' => 'Кандидаты'],
];
