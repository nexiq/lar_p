<?php
return [
    'form' => [
        'notifications' => [
            'saved' => 'Ваша анкета сохранена',
        ],
        'avatar' => [
            'title' => 'Аватар',
            'load' => 'Загрузить фотографию',
            'hint' => 'jpg, gif или png',
        ],
        'contact_info' => [
            'header' => 'Контактная информация',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'city_id' => 'Город проживания',
            'dob' => 'Дата рождения',
            'sex' => 'Пол',
        ],
        'individual' => [
            'header' => 'Индивидуальные особенности',
            'qualities' => 'Личные качества',
            'skills' => 'Навыки',
        ],
        'desired_work' => [
            'header' => 'Желаемая работа',
            'general_info' => 'Общая информация',
            'desired_salary' => 'Желаемая зарплата на руки',
            'salary_monthly' => 'За месяц',
            'salary_daily' => 'За день',
            'salary_hourly' => 'За час',
            'work_type' => 'Тип занятости',
            'work_schedule' => 'График работы',
            'work_team' => 'Атмосфера в коллективе',
            'work_relocation_ready' => 'Готовность к переезду',
            'trip_ready' => 'Готовность к командировкам',
        ],
        'desired_profarea' => [
            'header' => 'Желаемая профобласть',
            'profarea' => 'Профобласть',
            'add' => '+ Добавить желаемую профобласть',
            'remove' => 'Удалить',
            'confirm' => 'Вы точно хотите удалить желаемую профобласть?',
            'education_type' => 'Образование по профобласти',
            'experience' => 'Опыт работы по профобласти',
        ],
        'desired_speciality' => [
            'header' => 'Желаемая специальность',
            'speciality' => 'Специальность',
            'add' => '+ Добавить желаемую специальность',
            'remove' => 'Удалить',
            'confirm' => 'Вы точно хотите удалить желаемую специальность?',
            'education_type' => 'Образование по специальности',
            'experience' => 'Опыт работы по специальности',
        ],
        'experience' => [
            'header' => 'Опыт работы',
            'workplace' => 'Место работы',
            'add' => '+ Добавить место работы',
            'remove' => 'Удалить',
            'confirm' => 'Вы точно хотите удалить место работы?',
            'profarea' => 'Профобласть',
            'position' => 'Должность',
            'duration' => 'Стаж работы',
            'skills' => 'Отработанные навыки',
            'has_education' => 'Наличие профильного образования',
            'has_recommendation' => 'Наличие рекомендаций',
            'recommender_phone' => 'Телефон рекомендателя',
            'recommender_phone_ext' => 'доб.',
        ],
        'education' => [
            'header' => 'Образование',
            'place' => 'Место обучения',
            'add' => '+ Добавить место обучения',
            'remove' => 'Удалить',
            'confirm' => 'Вы точно хотите удалить место обучения?',
            'type' => 'Тип',
            'form' => 'Форма обучения',
            'place_name' => 'Название учреждения (не обязательно)',
            'speciality' => 'Специальность',
            'graduate_year' => 'Год окончания',
            'comment' => 'Текстовый комментарий',
        ],
        'other' => [
            'header' => 'Дополнительно',
            'height' => 'Рост, см',
            'body_type' => 'Телосложение',
            'clothing_size' => 'Размер одежды',
            'shoe_size' => 'Размер обуви',
            'is_smoking' => 'Курите ли Вы?',
            'languages' => 'Какими языками владеете?',
            'has_medical_book' => 'Наличие мед.книжки',
            'can_work_in_russia' => 'Разрешение на работу в РФ',
            'drivers_license' => 'Наличие водительских прав',
        ],
    ],
    'preview' => [
        'general' => [
            'years' => ':count год|:count года|:count лет',
        ],
    ],
    'catalog' => [
        'header' => 'Каталог резюме',
        'filter' => [
            'region' => 'Регион',
            'profarea' => 'Профобласть',
            'work_type' => 'Тип занятости',
            'work_team' => 'Атмосфера в коллективе',
        ],
    ],
    'moderation' => [
        'approve' => 'Принять',
        'decline' => 'Отклонить',
        'decline_reason' => 'Причина отказа',
        'no_requests' => 'Нет заявок на модерацию',
        'start' => 'Начать работу',
        'stop' => 'Закончить работу',
    ]
];
