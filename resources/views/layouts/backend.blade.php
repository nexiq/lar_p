<?php

use App\Models\Core\User;
use App\Models\Core\Role;
use App\Models\Content\Page;

?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name', 'Sfera') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if(auth()->user())
            <meta name="api_token" content="{{ auth()->user()->api_token }}">
        @endif()

        <!-- Scripts -->
        <script src="{{ mix('js/manifest.js') }}" defer></script>
        <script src="{{ mix('js/vendor.js') }}" defer></script>
        <script src="{{ mix('js/backend.js') }}" defer></script>

        <!-- Styles -->
        <link href="{{ asset('icofont/icofont.min.css') }}" rel="stylesheet">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/backend') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    <lang-switcher/>
                </div>
            </nav>
            <main class="py-4">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-body p-2">
                                    <admin-menu/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="card">
                                <div class="card-body">
                                    <router-view></router-view>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        @yield('init')
    </body>
</html>
