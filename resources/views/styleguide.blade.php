@extends('layouts.clean')

@section('content')
<div class="container">
    <h2>Colors</h2>

    <div class="row">
        <div class="col text-center">
            <div class="rounded-circle bg-dark d-inline-block" style="width:80px;height:80px;"></div>
            <div class="text-light">#2E3033</div>
        </div>
        <div class="col text-center">
            <div class="rounded-circle bg-light d-inline-block" style="width:80px;height:80px;"></div>
            <div class="text-light">#7C7F83</div>
        </div>
        <div class="col text-center">
            <div class="rounded-circle bg-primary d-inline-block" style="width:80px;height:80px;"></div>
            <div class="text-light">#3F4CB1</div>
        </div>
        <div class="col text-center">
            <div class="rounded-circle bg-success d-inline-block" style="width:80px;height:80px;"></div>
            <div class="text-light">#4CB13F</div>
        </div>
        <div class="col text-center">
            <div class="rounded-circle bg-danger d-inline-block" style="width:80px;height:80px;"></div>
            <div class="text-light">#F20000</div>
        </div>
        <div class="col text-center">
            <div class="rounded-circle bg-warning d-inline-block" style="width:80px;height:80px;"></div>
            <div class="text-light">#FFCC3D</div>
        </div>
    </div>

    <hr>
    <h2>Typography</h2>

    <div class="row">
        <div class="col-4">
            <h1>Heading 1</h1>
            <h2>Heading 2</h2>
            <h3>Heading 3</h3>
            <h4>Heading 4</h4>
            <h5>Heading 5</h5>
        </div>
        <div class="col-8">
            <h4>Paragraph</h4>
            <p>Таким образом укрепление и развитие структуры позволяет выполнять важные задания по разработке систем массового участия. Значимость этих проблем настолько очевидна, что реализация намеченных плановых заданий играет важную роль в формировании форм развития.</p>
            <h5>Link</h5>
            <a href="#">Какая-то ссылка</a>
        </div>
    </div>

    <hr>
    <h2>Buttons</h2>
    <div class="row">
        <div class="col">
            <h5>Button</h5>
            <a href="#" class="btn btn-primary">Кнопка с действием</a>
        </div>
        <div class="col">
            <h5>Disabled Button</h5>
            <a href="#" class="btn btn-primary disabled">Неактивная кнопка</a>
        </div>
    </div>

    <hr>
    <h2>Form</h2>
    <h3 class="text-muted">Inputs</h3>
    <div class="row mb-3">
        <div class="col">
            <input type="text" placeholder="Default" class="form-control">
        </div>
        <div class="col">
            <input type="text" placeholder="Success" class="form-control is-valid">
        </div>
        <div class="col">
            <input type="text" placeholder="Error" class="form-control is-invalid">
        </div>
        <div class="col">
            <input type="text" placeholder="Disabled" class="form-control" readonly>
        </div>
    </div>

    <h3 class="text-muted">Descriptive inputs</h3>
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>Username</label>
                <input type="text" placeholder="Default" class="form-control">
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label>Success</label>
                <input type="text" placeholder="Default" class="form-control is-valid">
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label>Error</label>
                <input type="text" placeholder="Default" class="form-control is-invalid">
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label>Disabled</label>
                <input type="text" placeholder="Default" class="form-control" readonly>
            </div>
        </div>
    </div>

    <hr>
    <h2>Textarea</h2>

    <div class="row">
        <div class="col">
            <label>Normal</label>
            <textarea class="form-control" rows="8"></textarea>
        </div>
        <div class="col">
            <label>Error</label>
            <textarea class="form-control is-invalid" rows="8"></textarea>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col">
            <h2>Checkbox</h2>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="cb1">
                    <label class="custom-control-label" for="cb1">Default</label>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="cb2" checked>
                    <label class="custom-control-label" for="cb2">Checked</label>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="cb3" disabled>
                    <label class="custom-control-label" for="cb3">Disabled</label>
                </div>
            </div>
        </div>
        <div class="col">
            <h2>Radio</h2>
            <div class="form-group">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="rad1" name="radio">
                    <label class="custom-control-label" for="rad1">Default</label>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="rad2" checked name="radio">
                    <label class="custom-control-label" for="rad2">Checked</label>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="rad3" disabled name="radio">
                    <label class="custom-control-label" for="rad3">Disabled</label>
                </div>
            </div>
        </div>
        <div class="col">
            <h2>Switch</h2>
            <div class="form-group">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="rad1">
                    <label class="custom-control-label" for="rad1">Off</label>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="rad2" checked>
                    <label class="custom-control-label" for="rad2">On</label>
                </div>
            </div>
        </div>
        <div class="col">
            <h2>Slider</h2>
            <input type="range" class="custom-range" value="0">
            <input type="range" class="custom-range" value="25">
            <input type="range" class="custom-range" value="75">
            <input type="range" class="custom-range" value="100">
        </div>
    </div>

    <hr>
    <h2>Cards</h2>
    <h5>Normal</h5>
    <div class="card">
        <div class="card-body">
            <div class="h2">Менеджер по продажам</div>
            <p class="card-text">
                Осталось тестирований: <strong>40</strong>
                Заканчиваются? <a href="#">Купить еще</a>
            </p>
            <div class="row">
                <div class="col-auto">
                    <a href="#" class="btn btn-primary">Пригласить соискателей</a>
                </div>
                <div class="col">
                    <div class="text-primary font-weight-bolder">
                        <span class="icofont-flag bg-primary text-white rounded-circle"></span>
                        10
                    </div>
                    <div>
                        Завершено
                    </div>
                </div>
                <div class="col">
                    <div class="text-success font-weight-bolder">
                        <span class="icofont-check bg-success text-white rounded-circle"></span>
                        5
                    </div>
                    <div>
                        Успешно
                    </div>
                </div>
                <div class="col">
                    <div class="text-danger font-weight-bolder">
                        <span class="icofont-error bg-danger text-white rounded-circle"></span>
                        3
                    </div>
                    <div>
                        Провалили
                    </div>
                </div>
                <div class="col">
                    <div class="text-warning font-weight-bolder">
                        <span class="icofont-minus bg-warning text-white rounded-circle"></span>
                        2
                    </div>
                    <div>
                        Проходят
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <h2>Notifications</h2>

    <div class="alert alert-primary">
        <span class="icofont-exclamation icofont-2x align-middle mr-1 my-n1 d-inline-block"></span>
        <span class="align-middle">Какой-нибудь текст</span>
        <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
        </button>
    </div>
    <div class="alert alert-success">
        <span class="icofont-check icofont-2x align-middle mr-1 my-n1 d-inline-block"></span>
        <span class="align-middle">Какой-нибудь текст об успешном действии</span>
        <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
        </button>
    </div>
    <div class="alert alert-danger">
        <span class="icofont-close icofont-2x align-middle mr-1 my-n1 d-inline-block"></span>
        <span class="align-middle">Предупреждение</span>
        <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
        </button>
    </div>

    <hr>
    <h2>Progress</h2>
    <div class="progress mb-2">
        <div class="progress-bar">0%</div>
    </div>
    <div class="progress mb-2">
        <div class="progress-bar" style="width: 25%">25%</div>
    </div>
    <div class="progress mb-2">
        <div class="progress-bar" style="width: 50%">50%</div>
    </div>
    <div class="progress mb-2">
        <div class="progress-bar" style="width: 75%">75%</div>
    </div>
    <div class="progress mb-2">
        <div class="progress-bar bg-success" style="width: 100%">Готово</div>
    </div>

    <hr>
    <h2>Tags</h2>
    <div class="row">
        <div class="col-auto">
            <h5>Normal</h5>
            <a href="#" class="btn btn-outline-primary">Normal</a>
        </div>
    </div>

    <hr>
    <h2>Pagination</h2>
    <ul class="pagination">
        <li class="page-item"><a class="page-link" href="#"><</a></li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item"><a class="page-link" href="#">></a></li>
    </ul>

    <h2>Dropdown</h2>
    <div class="dropdown">

        <div class="dropdown-menu show">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
        </div>
    </div>
    <hr>
</div>
@endsection
