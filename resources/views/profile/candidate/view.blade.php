@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="card">
            <div class="row">
                <div class="col-sm-8">
                    <div class="card-body">
                        <h1>
                            {{$profile->desiredSpecialities[0]->speciality->title}}
                        </h1>
                        <div class="row">
                            @if ($profile->avatarFile)
                            <div class="col-auto">
                                <img
                                    width="60" class="rounded-circle"
                                    src="{{$profile->avatarFile->url}}"
                                >
                            </div>
                            @endif
                            <div class="col">
                                <div class="h3">
                                    {{$profile->last_name}}
                                    {{$profile->first_name}}
                                    {{$profile->middle_name}}
                                </div>
                                <div>
                                    {{trans_choice('candidate.preview.general.years', $profile->age)}},
                                    {{$profile->city->name}}
                                </div>
                                <div>
                                    <i class="icofont-phone text-secondary"></i>
                                    {{$profile->user->phone}}
                                    {{$profile->user->phone_ext}}
                                </div>
                                <div>
                                    <i class="icofont-envelope text-secondary"></i>
                                    {{$profile->user->email}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="my-0">
                    <div class="card-body">
                        <div class="h3">
                            {{__('candidate.form.individual.header')}}
                        </div>
                        <table class="table table-borderless">
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.desired_work.work_relocation_ready')}}
                                </td>
                                <td>
                                    @if($profile->work_relocation_ready)
                                        {{$profile->relocationCity->name}}
                                    @else
                                        {{__('constants.common.yesno.no')}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.desired_work.trip_ready')}}
                                </td>
                                <td>
                                    @if($profile->trip_ready)
                                        {{$profile->tripCity->name}}
                                    @else
                                        {{__('constants.common.yesno.no')}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.individual.qualities')}}
                                </td>
                                <td>
                                    @foreach ($profile->qualities as $quality)
                                        &mdash; {{$quality->title}} <br>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.individual.skills')}}
                                </td>
                                <td>
                                    @foreach ($profile->skills as $skill)
                                        &mdash; {{$skill->title}} <br>
                                    @endforeach
                                </td>
                            </tr>
                        </table>
                    </div>
                    <hr class="my-0">
                    <div class="card-body">
                        <div class="h3">
                            {{__('candidate.form.desired_work.header')}}
                        </div>
                        <table class="table table-borderless">
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.desired_work.work_type')}}
                                </td>
                                <td>
                                    {{implode(', ', Helper::const('work.type', $profile->work_type, true))}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.desired_work.work_schedule')}}
                                </td>
                                <td>
                                    {{implode(', ', Helper::const('work.schedule', $profile->work_schedule, true))}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.desired_work.work_team')}}
                                </td>
                                <td>
                                    {{implode(', ', Helper::const('work.team', $profile->work_team, true))}}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <hr class="my-0">
                    <div class="card-body">
                        <div class="h3">
                            {{__('candidate.form.desired_profarea.header')}}
                        </div>
                        <div class="row">
                            @foreach ($profile->desiredProfareas as $desiredProfarea)
                                <div class="col-auto">
                                    <div class="alert alert-secondary text-dark">
                                        <div class="h4">
                                            {{$desiredProfarea->profarea->title}}
                                        </div>
                                        <table>
                                            <tr>
                                                <td class="text-muted pr-4">
                                                    {{__('candidate.form.education.header')}}
                                                </td>
                                                <td>
                                                    {{Helper::const('work.education_type', $desiredProfarea->education_type)}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-muted pr-4">
                                                    {{__('candidate.form.experience.header')}}
                                                </td>
                                                <td>
                                                    {{Helper::const('work.experience', $desiredProfarea->experience_type)}}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <hr class="my-0">
                    <div class="card-body">
                        <div class="h3">
                            {{__('candidate.form.desired_speciality.header')}}
                        </div>
                        <div class="row">
                            @foreach ($profile->desiredSpecialities as $desiredSpeciality)
                                <div class="col-auto">
                                    <div class="alert alert-secondary text-dark">
                                        <div class="h4">
                                            {{$desiredSpeciality->speciality->title}}
                                        </div>
                                        <table>
                                            <tr>
                                                <td class="text-muted pr-4">
                                                    {{__('candidate.form.education.header')}}
                                                </td>
                                                <td>
                                                    {{Helper::const('work.education_type', $desiredSpeciality->education_type)}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-muted pr-4">
                                                    {{__('candidate.form.experience.header')}}
                                                </td>
                                                <td>
                                                    {{Helper::const('work.experience', $desiredSpeciality->experience_type)}}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <hr class="my-0">
                    <div class="card-body">
                        <div class="h3">
                            {{__('candidate.form.education.header')}}
                        </div>
                        [[Education]]
                    </div>
                    <hr class="my-0">
                    <div class="card-body">
                        <div class="h3">
                            {{__('candidate.form.experience.header')}}
                        </div>
                        [[Experience]]
                    </div>
                    <hr class="my-0">
                    <div class="card-body">
                        <div class="h3">
                            {{__('candidate.form.other.header')}}
                        </div>
                        <table>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.other.height')}}
                                </td>
                                <td>
                                    {{$profile->height}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.other.body_type')}}
                                </td>
                                <td>
                                    {{Helper::const('work.body_type', $profile->body_type)}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.other.clothing_size')}}
                                </td>
                                <td>
                                    {{Helper::const('work.clothing_size', $profile->clothing_size)}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.other.shoe_size')}}
                                </td>
                                <td>
                                    {{$profile->shoe_size}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.other.languages')}}
                                </td>
                                <td>
                                    {{implode(', ', $profile->languages->pluck('title')->all())}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.other.is_smoking')}}
                                </td>
                                <td>
                                    {{Helper::const('common.yesno', $profile->is_smoking)}}
                                </td>
                            </tr>
                            <tr>
                                <td class="text-muted pr-4">
                                    {{__('candidate.form.other.drivers_license')}}
                                </td>
                                <td>
                                    {{implode(', ', Helper::const('work.drivers_license', $profile->drivers_license, true))}}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card-body">
                        @can('update', $profile->resource)
                            <a
                                class="btn btn-secondary"
                                href="{{route('candidate.edit', ['profile'=>$profile])}}"
                            >
                                {{__('controls.button.edit')}}
                            </a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
