@extends('layouts.main')

@section('content')
<div class="container">
    <h1>{{__('candidate.catalog.header')}}</h1>

    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <label>{{__('candidate.catalog.filter.region')}}</label>
                    <div class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" id="filter_region">
                        <label class="custom-control-label" for="filter_region">
                            [[Москва]]
                        </label>
                    </div>
                    <div class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" id="filter_relocation_ready">
                        <label class="custom-control-label" for="filter_relocation_ready">
                            {{__('candidate.form.desired_work.work_relocation_ready')}}
                        </label>
                    </div>
                    <div class="custom-control custom-checkbox mb-2">
                        <input type="checkbox" class="custom-control-input" id="filter_trip_ready">
                        <label class="custom-control-label" for="filter_trip_ready">
                            {{__('candidate.form.desired_work.trip_ready')}}
                        </label>
                    </div>
                </div>
                <hr class="my-0">
                <div class="card-body">
                    <label>{{__('candidate.catalog.filter.profarea')}}</label>
                    <select class="custom-select">
                        <option selected>{{__('controls.label.select')}}</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
                <hr class="my-0">
                <div class="card-body">
                    <label>{{__('candidate.catalog.filter.work_type')}}</label>
                    @foreach (config('const.work.type') as $key => $value)
                    <div class="custom-control custom-checkbox mb-1">
                        <input
                            type="checkbox" class="custom-control-input"
                            id="filter_work_type_{{$value}}"
                        >
                        <label class="custom-control-label" for="filter_work_type_{{$value}}">
                            {{__("constants.work.type.$key")}}
                        </label>
                    </div>
                    @endforeach
                </div>
                <hr class="my-0">
                <div class="card-body">
                    <label>{{__('candidate.catalog.filter.work_team')}}</label>
                    @foreach (config('const.work.team') as $key => $value)
                    <div class="custom-control custom-checkbox mb-1">
                        <input
                            type="checkbox" class="custom-control-input"
                            id="filter_work_team_{{$value}}"
                        >
                        <label class="custom-control-label" for="filter_work_team_{{$value}}">
                            {{__("constants.work.team.$key")}}
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <input class="form-control" placeholder="{{__('controls.label.search_query')}}">

            @foreach ($profiles as $profile)
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-9">
                                <div class="h3">
                                    <a href="{{route('candidate.view', ['profile' => $profile])}}">
                                        {{$profile->desiredSpecialities[0]->speciality->title}}
                                    </a>
                                </div>
                                <div>
                                    {{trans_choice('candidate.preview.general.years', $profile->age)}},
                                    {{$profile->city->name}}
                                </div>
                                <table class="table table-borderless table-sm">
                                    <tr>
                                        <td class="text-muted pl-0">
                                            {{__('candidate.form.desired_work.work_type')}}
                                        </td>
                                        <td>
                                            {{implode(', ', Helper::const('work.type', $profile->work_type, true))}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted pl-0">
                                            {{__('candidate.form.desired_work.work_schedule')}}
                                        </td>
                                        <td>
                                            {{implode(', ', Helper::const('work.schedule', $profile->work_schedule, true))}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted pl-0">
                                            {{__('candidate.form.desired_work.work_relocation_ready')}}
                                        </td>
                                        <td>
                                            @if($profile->work_relocation_ready)
                                                {{$profile->relocationCity->name}}
                                            @else
                                                {{__('constants.common.yesno.no')}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted pl-0">
                                            {{__('candidate.form.desired_profarea.profarea')}}
                                        </td>
                                        <td>
                                            {{implode(', ', $profile->desiredProfareas->pluck('profarea.title')->all())}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-muted pl-0">
                                            {{__('candidate.form.individual.qualities')}}
                                        </td>
                                        <td>
                                            {{implode(', ', $profile->qualities->pluck('title')->all())}}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-3">
                                <div class="h2 mb-0">
                                    {{$profile->salary_monthly}}
                                    <i class="icofont-rouble"></i>
                                </div>
                                <div class="text-muted">
                                    <small>{{__('candidate.form.desired_work.salary_monthly')}}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
