<?php

use App\Services\Content\TranslationService;
use App\Http\Resources\Core\UserResource;
use App\Http\Resources\Profile\CandidateResource;

$locale = app()->getLocale();
$service = new TranslationService();

?>

@extends('layouts.app')

@section('content')
    <router-view/>
@endsection

@section('init')
    <script>
        window.user = <?php
            echo json_encode(
                new UserResource(auth()->user()),
                JSON_UNESCAPED_UNICODE
            )
        ?>;
        window.locales = <?php
            echo json_encode($service->getAvailableLocales());
        ?>;
        window.translations = <?php
            echo json_encode([
                'locale' => $locale,
                'messages' => [
                    $locale => $service->getTranslation($locale)
                ],
            ], JSON_UNESCAPED_UNICODE);
        ?>;
        window.constants = <?php
            echo json_encode(config('const'));
        ?>;
        window.profile = <?php
            echo json_encode(
                new CandidateResource($profile),
                JSON_UNESCAPED_UNICODE
            );
        ?>;
        window.previewLink = "<?php
            echo route('candidate.view', ['profile' => $profile]);
        ?>";
    </script>
@endsection
