@extends('layouts.app')

@section('title'){{e($page->title)}}@endsection

@section('meta-description')
    <meta name="description" content="{{e($page->description)}}">
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            {!! parsedown($page->content) !!}
        </div>
    </div>
</div>
@endsection

@section('langSwitcher')
<li class="nav-item dropdown">
    <a href="#" class="nav-item nav-link dropdown-toggle" data-toggle="dropdown">
        {{app()->getLocale()}}
    </a>
    <div class="dropdown-menu">
        @foreach (config('translatable.locales') as $locale)
            @if ($locale != app()->getLocale())
                <a class="dropdown-item" href="{{route('page', [
                    'lang' => ($locale == 'ru' ? '' : $locale),
                    'page' => $page
                ])}}">
                    {{$locale}}
                </a>
            @endif
        @endforeach
    </div>
</li>
@endsection
