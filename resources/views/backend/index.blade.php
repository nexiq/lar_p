<?php

use App\Services\Content\TranslationService;
use App\Http\Resources\Core\UserResource;

$locale = app()->getLocale();
$service = new TranslationService();

?>

@extends('layouts.backend')

@section('init')
    <script>
        window.user = <?php
            echo json_encode(
                new UserResource(auth()->user()),
                JSON_UNESCAPED_UNICODE
            )
        ?>;
        window.locales = <?php
            echo json_encode($service->getAvailableLocales());
        ?>;
        window.translations = <?php
            echo json_encode([
                'locale' => $locale,
                'messages' => [
                    $locale => $service->getTranslation($locale)
                ],
            ], JSON_UNESCAPED_UNICODE);
        ?>;
    </script>
@endsection
