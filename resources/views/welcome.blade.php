<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Sfera</title>

        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ route('welcome') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <ul class="navbar-nav">
                    @if (Route::has('login'))
                        @auth
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/home') }}">Home</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Login</a>
                            </li>

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Register</a>
                                </li>
                            @endif
                        @endauth
                    @endif
                </ul>
            </div>
        </nav>

        <div class="container mt-5">
            <div class="row justify-content-center">
                <div class="h1">
                    Sfera v0.0.0
                </div>
            </div>

            <div class="row justify-content-center">
                <a href="https://gitlab.com/sfera-rabota/site/blob/master/readme.md" target="_blank">Repository</a> |
                <a href="{{url('/styleguide')}}">Styleguide</a> |
                <a href="{{route('backend')}}">Backend</a> |
                <a href="{{route('candidate.index')}}">Каталог</a>
            </div>
        </div>
    </body>
</html>
