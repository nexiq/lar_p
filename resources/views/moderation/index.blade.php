@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="col-md-12">
                    @if ($isWorking)
                        <a href="{{route('moderation.stop')}}"
                           class="btn btn-danger">{{__('candidate.moderation.stop')}}</a>
                    @else
                        <a href="{{route('moderation.start')}}"
                           class="btn btn-success">{{__('candidate.moderation.start')}}</a>
                    @endif
                </div>
            </div>
        </div>
        <br>

        @if ($isWorking)
            <div class="row">


                <div class="container">
                    @if (!$profile)
                        {{__('candidate.moderation.no_requests')}}
                    @else
                        <div class="card">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="card-body">
                                        <h1>
                                            {{$profile->desiredSpecialities[0]->speciality->title}}
                                        </h1>
                                        <div class="row">
                                            @if ($profile->avatarFile)
                                                <div class="col-auto">
                                                    <img
                                                        width="60" class="rounded-circle"
                                                        src="{{$profile->avatarFile->url}}"
                                                    >
                                                </div>
                                            @endif
                                            <div class="col">
                                                <div class="h3">
                                                    {{$profile->last_name}}
                                                    {{$profile->first_name}}
                                                    {{$profile->middle_name}}
                                                </div>
                                                <div>
                                                    <i class="icofont-phone text-secondary"></i>
                                                    {{$profile->user->phone}}
                                                    {{$profile->user->phone_ext}}
                                                </div>
                                                <div>
                                                    <i class="icofont-envelope text-secondary"></i>
                                                    {{$profile->user->email}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="my-0">
                                    <div class="col">
                                        <br>
                                        <form action="{{url('decline')}}" method="post">
                                            <a href="{{url('approve')}}/{{$profile->id}}"
                                               class="btn btn-success">{{__('candidate.moderation.approve')}}</a>
                                            <button type="submit"
                                                    class="btn btn-danger">{{__('candidate.moderation.decline')}}</button>

                                            {{csrf_field()}}
                                            <input name="id" type="hidden" value="{{$profile->id}}">
                                            <br><br>
                                            <select class="form-control" id="templates">
                                                @foreach ($templates as $template)
                                                    <option>{{ $template->name }}</option>
                                                @endforeach
                                            </select>
                                            <br>
                                            <textarea placeholder="{{__('candidate.moderation.decline_reason')}}"
                                                      class="form-control" name="reason" id="reason"></textarea>
                                        </form>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

            </div>

            <script>
                var max_time = {{$maxTime}} -(60 * 1000);
                var deadline_canceled = false;
                window.onload = function () {
                    var select = document.getElementById('templates');
                    var reason = document.getElementById('reason');
                    select.onchange = function () {
                        reason.value = select.value;
                    };

                    function deadline() {
                        var cancel_deadline = confirm("Время на модерацию истекает, хотите продолжить?");
                        if (!cancel_deadline) {
                            window.location = '{{url('moderation/stop')}}'
                        }
                        setTimeout(function () {
                            if (!deadline_canceled) {
                                window.location = '{{url('moderation/stop')}}'
                            }
                            var deadline_canceled = true;
                        }, max_time);
                    }

                    setTimeout(function () {
                        deadline()
                    }, max_time);
                }
            </script>
        @endif
    </div>
@endsection
