<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::get('me', 'Api\Core\UserController@me');
    Route::apiResource('users', 'Api\Core\UserController');
    Route::apiResource('roles', 'Api\Core\RoleController');
    Route::apiResource('permissions', 'Api\Core\PermissionController');

    Route::get('translations', 'Api\Content\TranslationController@index');
    Route::get('translations/{locale}', 'Api\Content\TranslationController@show');
    Route::put('translations/{locale}', 'Api\Content\TranslationController@update');

    Route::apiResource('pages', 'Api\Content\PageController');
    Route::apiResource('qualities', 'Api\Work\QualityController');
    Route::apiResource('skills', 'Api\Work\SkillController');
    Route::apiResource('profareas', 'Api\Work\ProfareaController');
    Route::apiResource('regions', 'Api\Work\RegionController');
    Route::apiResource('cities', 'Api\Work\CityController');
    Route::apiResource('countries', 'Api\Work\CountryController');
    Route::apiResource('specialities', 'Api\Work\SpecialityController');
    Route::apiResource('positions', 'Api\Work\PositionController');
    Route::apiResource('languages', 'Api\Work\LanguageController');

    Route::group([
        'prefix' => 'profile',
    ], function () {
        Route::apiResource('candidates', 'Api\Profile\CandidateApiController');
        Route::post('avatar/{candidate}', 'Api\Profile\CandidateApiController@uploadAvatar');
    });
});
