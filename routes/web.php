<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/backend', 'BackendController@index')->name('backend');

Route::get('/candidates', 'Profile\CandidateController@index')->name('candidate.index');
Route::get('/candidate/{profile}', 'Profile\CandidateController@view')->name('candidate.view');
Route::get('/profile/{profile}', 'Profile\CandidateController@edit')->name('candidate.edit');

Route::get('/moderation', 'Moderation\ModerationController@index');
Route::get('/approve/{profile}', 'Moderation\ModerationController@approve')->name('moderation.approve');
Route::post('/decline', 'Moderation\ModerationController@decline')->name('moderation.decline');
Route::get('/moderation/start', 'Moderation\ModerationController@start')->name('moderation.start');
Route::get('/moderation/stop', 'Moderation\ModerationController@stop')->name('moderation.stop');


Route::view('/styleguide', 'styleguide');

// SHOULD BE LAST!
Route::get('/{page}', 'PageController@index')->name('page');
