<?php

use App\Models\Core\Permission;
use App\Models\Core\Role;
use App\Models\Core\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EntrustSeeder extends Seeder
{
    /**
     * @var Role
     */
    protected $adminRole;

    /**
     * @var Role
     */
    protected $candidateRole;
    protected $permissions = [];

    /**
     * Run the database seeds.
     * Creates roles and permission, creates admin user
     * and assigns 'admin' role to the created admin user
     *
     * @return void
     */
    public function run()
    {
        $this->createRoles();
        $this->createPermissions();
        $this->createPermissionsForCandidateProfile();


        $this->createAdmin();
        $this->assignPermissionsToAdminRole();
        $this->assignPermissionsToCandidateRole();

        $this->createAdmin();
    }

    protected function assignPermissionsToCandidateRole()
    {
        $this->candidateRole->detachPermissions(Permission::get());
        $this->candidateRole->attachPermission(Permission::where('name', 'content.files.create')->first());
        $this->candidateRole->attachPermission(Permission::where('name', 'content.files.delete')->first());
    }

    protected function assignPermissionsToAdminRole()
    {
        $this->adminRole->detachPermissions(Permission::get());
        $this->adminRole->attachPermissions($this->permissions);
    }

    /**
     * Создает права для профайла соискателя
     */
    protected function createPermissionsForCandidateProfile()
    {
        // Skills
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'profile.candidates.read',
        ], [
            'description' => 'читать профайлы'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'profile.candidates.create',
        ], [
            'description' => 'создавать профайлы'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'profile.candidates.update',
        ], [
            'description' => 'редактировать профайлы'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'profile.candidates.delete',
        ], [
            'description' => 'удалять профайлы'
        ]);
    }

    /**
     * Creates roles
     * @return void
     */
    public function createRoles()
    {
        $this->adminRole = Role::firstOrCreate([
            'name' => 'admin',
        ], [
            'display_name' => 'Администратор',
            'description' => 'Полный доступ к системе',
        ]);

        Role::firstOrCreate([
            'name' => 'manager',
        ], [
            'display_name' => 'Менеджер',
            'description' => 'Управляет пользователями',
        ]);

        Role::firstOrCreate([
            'name' => 'moderator',
        ], [
            'display_name' => 'Модератор',
            'description' => 'Модерирует вакансии и резюме',
        ]);

        Role::firstOrCreate([
            'name' => 'content',
        ], [
            'display_name' => 'Контент-менеджер',
            'description' => 'Редактирует статический контент',
        ]);

        Role::firstOrCreate([
            'name' => 'translator',
        ], [
            'display_name' => 'Переводчик',
            'description' => 'Переводит тексты на другие языки',
        ]);

        Role::firstOrCreate([
            'name' => 'employer',
        ], [
            'display_name' => 'Работодатель',
            'description' => 'Сотрудник работодателя. Размещает вакансии',
        ]);

        $this->candidateRole = Role::firstOrCreate([
            'name' => 'candidate',
        ], [
            'display_name' => 'Соискатель',
            'description' => 'Ищет работу, проходит тесты',
        ]);
    }

    /**
     * Creates admin user
     */
    public function createAdmin()
    {
        $admin = User::firstOrCreate([
            'email' => env('ADMIN_EMAIL', 'admin@example.com'),
        ], [
            'name' => env('ADMIN_NAME', 'admin'),
            'password' => Hash::make(env('ADMIN_PASSWORD', 'password')),
            'api_token' => Str::random(60),
        ]);

        $admin->detachRoles(Role::all());
        $admin->attachRole($this->adminRole);
    }

    /**
     * Creates permissions
     * @return void
     */
    public function createPermissions()
    {
        // files
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.files.read',
        ], [
            'display_name' => 'Просмотр файлов',
            'description' => 'Просмотр файлов',
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.files.create',
        ], [
            'display_name' => 'Создание файлов',
            'description' => 'Создание файлов',
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.files.update',
        ], [
            'display_name' => 'Обновление файлов',
            'description' => 'Обновление файлов',
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.files.delete',
        ], [
            'display_name' => 'Удаление файлов',
            'description' => 'Удаление файлов',
        ]);
        // Personal data
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'core.personal.read',
        ], [
            'display_name' => 'Просмотр ПД',
            'description' => 'просматривать персональные данные',
        ]);

        // Users
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'core.users.read',
        ], [
            'description' => 'читать список пользователей'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'core.users.create',
        ], [
            'description' => 'создавать пользователей'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'core.users.update',
        ], [
            'description' => 'редактировать пользователей'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'core.users.delete',
        ], [
            'description' => 'удалять пользователей'
        ]);

        // Roles
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'core.roles.read',
        ], [
            'description' => 'читать список ролей'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'core.roles.update',
        ], [
            'description' => 'редактировать роли'
        ]);

        // Permissions
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'core.permissions.read',
        ], [
            'description' => 'читать список разрешений'
        ]);

        // Pages
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.pages.read',
        ], [
            'description' => 'читать список страниц'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.pages.create',
        ], [
            'description' => 'создавать страницы'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.pages.update',
        ], [
            'description' => 'редактировать страницы'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.pages.delete',
        ], [
            'description' => 'удалять страницы'
        ]);

        // Translations
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.translations.update',
        ], [
            'description' => 'Редактировать перевод'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'content.translations.read',
        ], [
            'description' => 'Читать переводы'
        ]);

        // Qualities
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.qualities.read',
        ], [
            'description' => 'читать список качеств'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.qualities.create',
        ], [
            'description' => 'создавать качества'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.qualities.update',
        ], [
            'description' => 'редактировать качества'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.qualities.delete',
        ], [
            'description' => 'удалять качества'
        ]);

        // Skills
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.skills.read',
        ], [
            'description' => 'читать список навыков'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.skills.create',
        ], [
            'description' => 'создавать навыки'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.skills.update',
        ], [
            'description' => 'редактировать навыки'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.skills.delete',
        ], [
            'description' => 'удалять навыки'
        ]);

        // Profareas
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.profareas.read',
        ], [
            'description' => 'читать список профобластей'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.profareas.create',
        ], [
            'description' => 'создавать профобласти'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.profareas.update',
        ], [
            'description' => 'редактировать профобласти'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.profareas.delete',
        ], [
            'description' => 'удалять профобласти'
        ]);

        // Countries
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.countries.read',
        ], [
            'description' => 'читать список стран'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.countries.create',
        ], [
            'description' => 'создавать страны'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.countries.update',
        ], [
            'description' => 'редактировать страны'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.countries.delete',
        ], [
            'description' => 'удалять страны'
        ]);

        // Regions
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.regions.read',
        ], [
            'description' => 'читать список регионов'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.regions.create',
        ], [
            'description' => 'создавать регионы'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.regions.update',
        ], [
            'description' => 'редактировать регионы'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.regions.delete',
        ], [
            'description' => 'удалять регионы'
        ]);

        // Cities
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.cities.read',
        ], [
            'description' => 'читать список городов'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.cities.create',
        ], [
            'description' => 'создавать города'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.cities.update',
        ], [
            'description' => 'редактировать города'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.cities.delete',
        ], [
            'description' => 'удалять города'
        ]);

        // Specialities
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.specialities.read',
        ], [
            'description' => 'читать список специальностей'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.specialities.create',
        ], [
            'description' => 'создавать специальности'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.specialities.update',
        ], [
            'description' => 'редактировать специальности'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.specialities.delete',
        ], [
            'description' => 'удалять специальности'
        ]);

        // Positions
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.positions.read',
        ], [
            'description' => 'читать список должностей'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.positions.create',
        ], [
            'description' => 'создавать должности'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.positions.update',
        ], [
            'description' => 'редактировать должности'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.positions.delete',
        ], [
            'description' => 'удалять должности'
        ]);

        // Languages
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.languages.read',
        ], [
            'description' => 'читать список языков'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.languages.create',
        ], [
            'description' => 'создавать языки'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.languages.update',
        ], [
            'description' => 'редактировать языки'
        ]);
        $this->permissions[] = Permission::firstOrCreate([
            'name' => 'work.languages.delete',
        ], [
            'description' => 'удалять языки'
        ]);
    }
}
