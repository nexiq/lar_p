<?php

use App\Models\Core\User;
use App\Models\Profile\CandidateProfile;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        factory(User::class, 20)->create();
        factory(CandidateProfile::class, 20)->create()->each(function ($profile) use ($faker) {
            $profile->skills()->sync($faker->randomElements(range(1,50), $count = 3));
            $profile->qualities()->sync($faker->randomElements(range(1,50), $count = 3));
            $profile->languages()->sync($faker->randomElements(range(1,50), $count = 3));
            $profile->desiredProfareas()->create([
                'profarea_id' => $faker->numberBetween(1, 50),
                'education_type' => $faker->randomElement(array_values(config('const.work.education_type'))),
                'experience_type' => $faker->randomElement(array_values(config('const.work.experience'))),
            ]);
            $profile->desiredSpecialities()->create([
                'speciality_id' => $faker->numberBetween(1, 50),
                'education_type' => $faker->randomElement(array_values(config('const.work.education_type'))),
                'experience_type' => $faker->randomElement(array_values(config('const.work.experience'))),
            ]);
        });
    }
}
