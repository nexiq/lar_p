<?php

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EntrustSeeder::class,
            EntitiesSeeder::class,
            UsersSeeder::class,
        ]);
    }
}
