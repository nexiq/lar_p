<?php

use App\Models\Profile\ModerationAnswerTemplate;
use App\Models\Work\City;
use App\Models\Work\Profarea;
use App\Models\Work\Quality;
use App\Models\Work\Region;
use App\Models\Work\Country;
use App\Models\Work\Skill;
use App\Models\Work\Speciality;
use App\Models\Work\Position;
use App\Models\Work\Language;
use Illuminate\Database\Seeder;

class EntitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Country::class, 50)->create();
        factory(Region::class, 50)->create();
        factory(City::class, 50)->create();
        factory(Profarea::class, 50)->create();
        factory(Quality::class, 50)->create();
        factory(Skill::class, 50)->create();
        factory(Speciality::class, 50)->create();
        factory(Position::class, 50)->create();
        factory(Language::class, 50)->create();
        factory(ModerationAnswerTemplate::class, 50)->create();
    }
}
