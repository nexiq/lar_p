<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_profile_id')->unsigned();

            $table->integer('type')->unsigned();
            $table->integer('form')->unsigned();
            $table->string('place_name')->nullable();
            $table->bigInteger('speciality_id')->unsigned();
            $table->integer('graduate_year')->unsigned();
            $table->text('comment')->nullable();

            $table
                ->foreign('candidate_profile_id')
                ->references('id')
                ->on('candidate_profiles')
                ->onDelete('cascade');
            $table
                ->foreign('speciality_id')
                ->references('id')
                ->on('specialities')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('educations');
    }
}
