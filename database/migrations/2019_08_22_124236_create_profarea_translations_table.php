<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfareaTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profarea_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('profarea_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title', 100);

            $table->unique(['profarea_id', 'locale']);
            $table
                ->foreign('profarea_id')
                ->references('id')
                ->on('profareas')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profarea_translations');
    }
}
