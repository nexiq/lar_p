<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModerationAnswerTemplateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moderation_answer_template_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('moderation_answer_template_id')->unsigned();
            $table->string('locale')->index();
            $table->string('name', 100);

            $table->unique(['moderation_answer_template_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moderation_answer_template_translations');
    }
}
