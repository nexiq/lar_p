<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQualityTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quality_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('quality_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title', 100);

            $table->unique(['quality_id', 'locale']);
            $table
                ->foreign('quality_id')
                ->references('id')
                ->on('qualities')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quality_translations');
    }
}
