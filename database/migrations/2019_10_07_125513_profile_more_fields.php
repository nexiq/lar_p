<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfileMoreFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidate_profiles', function (Blueprint $table) {
            $table->boolean('trip_ready')->nullable();
            $table->bigInteger('trip_city_id')->unsigned()->nullable();
            $table->integer('drivers_license')->unsigned()->nullable();

            $table
                ->foreign('trip_city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidate_profiles', function (Blueprint $table) {
            $table->dropForeign('candidate_profiles_trip_city_id_foreign');
        });

        Schema::table('candidate_profiles', function (Blueprint $table) {
            $table->dropColumn([
                'trip_ready',
                'trip_city_id',
                'drivers_license',
            ]);
        });
    }
}
