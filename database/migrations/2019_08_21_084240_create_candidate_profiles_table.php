<?php

use App\Services\Content\UploadService\UploadService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->unique();
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('middle_name', 100)->nullable();

            $table->date('dob')->nullable();
            $table->boolean('sex')->nullable();

            $table->unsignedSmallInteger('height')->nullable();
            $table->unsignedSmallInteger('weight')->nullable();
            $table->unsignedSmallInteger('clothing_size')->nullable();
            $table->unsignedSmallInteger('shoe_size')->nullable();
            $table->unsignedSmallInteger('body_type')->nullable();
            $table->boolean('is_smoking')->nullable();
            $table->boolean('has_medical_book')->nullable();
            $table->boolean('can_work_in_russia')->nullable();


            UploadService::uploadFileColumn($table, 'resume_file_id');
            UploadService::uploadFileColumn($table, 'avatar_file_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_profiles');
    }
}
