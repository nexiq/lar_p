<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workplaces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('candidate_profile_id');
            $table->unsignedBigInteger('profarea_id');
            $table->unsignedBigInteger('position_id');
            $table->unsignedInteger('duration');
            $table->boolean('has_education');
            $table->boolean('has_recommendation');
            $table->string('recommender_phone')->nullable();
            $table->string('recommender_phone_ext')->nullable();

            $table
                ->foreign('candidate_profile_id')
                ->references('id')
                ->on('candidate_profiles')
                ->onDelete('cascade');
            $table
                ->foreign('profarea_id')
                ->references('id')
                ->on('profareas')
                ->onDelete('cascade');
            $table
                ->foreign('position_id')
                ->references('id')
                ->on('positions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workplaces');
    }
}
