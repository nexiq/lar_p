<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CandidateProfileLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_profile_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_profile_id')->unsigned();
            $table->bigInteger('language_id')->unsigned();

            $table
                ->foreign('candidate_profile_id')
                ->references('id')
                ->on('candidate_profiles')
                ->onDelete('cascade');
            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade');

            $table->unique([
                'candidate_profile_id',
                'language_id',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_profile_languages');
    }
}
