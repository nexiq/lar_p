<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialityTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speciality_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('speciality_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title', 100);

            $table->unique(['speciality_id', 'locale']);
            $table
                ->foreign('speciality_id')
                ->references('id')
                ->on('specialities')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speciality_translations');
    }
}
