<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileModerationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_moderations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('candidate_profile_id');
            $table->bigInteger('user_id');
            $table->timestamp('deadline_at')->nullable();
            $table->timestamps();

            $table->foreign('candidate_profile_id')
                ->references('id')
                ->on('candidate_profiles')
                ->onDelete('set null');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_moderations');
    }
}
