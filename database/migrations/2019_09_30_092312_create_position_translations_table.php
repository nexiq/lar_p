<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('position_id')->unsigned();
            $table->string('locale')->index();
            $table->string('title', 100);

            $table->unique(['position_id', 'locale']);
            $table
                ->foreign('position_id')
                ->references('id')
                ->on('positions')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_translations');
    }
}
