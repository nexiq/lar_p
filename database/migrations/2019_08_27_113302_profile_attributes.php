<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfileAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('candidate_profiles', function (Blueprint $table) {
            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null');

            $table->unsignedInteger('salary_monthly')->nullable();
            $table->unsignedInteger('salary_daily')->nullable();
            $table->unsignedInteger('salary_hourly')->nullable();

            $table->unsignedInteger('work_type')->nullable()->comment('Тип занятости');
            $table->unsignedInteger('work_schedule')->nullable()->comment('График работы');
            $table->unsignedInteger('work_team')->nullable()->comment('Атмосфера в коллективе');
            $table->boolean('work_relocation_ready')->nullable()->comment('Готовность к переезду');

            $table->bigInteger('relocation_city_id')->unsigned()->nullable();
            $table->foreign('relocation_city_id')->references('id')->on('cities')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('candidate_profiles', function (Blueprint $table) {
            $table->dropForeign('candidate_profiles_city_id_foreign');
        });
        Schema::table('candidate_profiles', function (Blueprint $table) {
            $table->dropColumn('city_id');
        });
        Schema::table('candidate_profiles', function (Blueprint $table) {
            $table->dropForeign('candidate_profiles_relocation_city_id_foreign');
        });
        Schema::table('candidate_profiles', function (Blueprint $table) {
            $table->dropColumn('relocation_city_id');
        });

        Schema::table('candidate_profiles', function (Blueprint $table) {
            $table->dropColumn([
                'salary_monthly',
                'salary_daily',
                'salary_hourly',
                'work_type',
                'work_schedule',
                'work_team',
                'work_relocation_ready',

            ]);
        });
    }
}
