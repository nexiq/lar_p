<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkplaceSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workplace_skills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('workplace_id')->unsigned();
            $table->bigInteger('skill_id')->unsigned();

            $table
                ->foreign('workplace_id')
                ->references('id')
                ->on('workplaces')
                ->onDelete('cascade');
            $table
                ->foreign('skill_id')
                ->references('id')
                ->on('skills')
                ->onDelete('cascade');

            $table->unique([
                'workplace_id',
                'skill_id',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workplace_skills');
    }
}
