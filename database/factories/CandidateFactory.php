<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile\CandidateProfile;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CandidateProfile::class, function (Faker $faker) {
    return [
        'user_id' => $faker->unique()->numberBetween(1, 20),
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName(),
        'dob' => $faker->date('d.m.Y'),
        'sex' => $faker->randomElement([0,1]),
        'is_smoking' => $faker->randomElement([0,1]),
        'has_medical_book' => $faker->randomElement([0,1]),
        'can_work_in_russia' => $faker->randomElement([0,1]),

        'height' => $faker->numberBetween(100, 200),
        'weight' => $faker->numberBetween(40, 150),
        'clothing_size' => $faker->randomElement(array_values(config('const.work.clothing_size'))),
        'shoe_size' => $faker->numberBetween(30, 50),
        'body_type' => $faker->randomElement(array_values(config('const.work.body_type'))),

        'city_id' => $faker->numberBetween(1, 50),

        'salary_monthly' => $faker->numberBetween(10000, 500000),
        'salary_daily' => $faker->numberBetween(500, 2500),
        'salary_hourly' => $faker->numberBetween(50, 250),
        'work_type' => $faker->numberBetween(0, 32),
        'work_schedule' => $faker->numberBetween(0, 4),
        'work_team' => $faker->numberBetween(0, 16),

        'work_relocation_ready' => $faker->randomElement([0,1]),
        'relocation_city_id' => $faker->numberBetween(1, 50),

        'trip_ready' => $faker->randomElement([0,1]),
        'trip_city_id' => $faker->numberBetween(1, 50),
        'drivers_license' => $faker->numberBetween(0, 16),
    ];
});
