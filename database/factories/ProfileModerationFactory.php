<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile\CandidateProfile;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CandidateProfile::class, function (Faker $faker) {
    return [
        'candidate_profile_id' => $faker->unique()->numberBetween(1, 20),
        'user_id' => $faker->unique()->numberBetween(1, 20),
    ];
});
