<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Content\Page;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Page::class, function (Faker $faker) {
    return [
        'slug' => uniqid(),
        'title' => $faker->sentence(3),
        'description' => $faker->text(),
        'content' => $faker->text()
    ];
});
